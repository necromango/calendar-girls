<?php

session_start();
require_once('includes/db_worx.php');
require_once('includes/cgops.php');
require_once('includes/formhelpers.php');
$on_shift_list = 0;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title>Calendar Girls</title>
<link rel=stylesheet type="text/css" href="style/cgirlsstyle.css">

</head>
<body>

<div align="left" class="mainbox">
	<?php
		if(!$_SESSION['username']) {		//	If the user is not logged in
											//	Just give them a link to the log in page	
			echo	'<p class = "centre_cell"><a href="log_in.php">Log in</a></p>';
		}
		else{		//	Otherwise - carry on ...
			menubox();
			?>
			<table border = "2" width = "100%">
				<tr>
					<td>
						<div class = "centre_cell">
						<?php

		}

					?>
					
	<h2>The Diver's mobile Phone number is 027 944 1366</h2>

	<p>
		Driver's fee for girls to and from home - $15
	</p>

	<h2>Which driver to call</h2>					
	<table class="drivers_table">
	
		<tr class="drivers_header"><th>Day</th><th>Driver 1</th><th>Driver2</th><th>Driver 3</th></tr>
		<tr class="drivers_row"><td class="drivers_td">Monday</td><td class="drivers_td">Tony</td><td class="drivers_td">Homz</td><td class="drivers_td">Lenny</td></tr>
		<tr class="drivers_row"><td class="drivers_td">Tuesday</td><td class="drivers_td">Lenny</td><td class="drivers_td">Tony</td><td class="drivers_td">Homz</td></tr>
		<tr class="drivers_row"><td class="drivers_td">Wednesday</td><td class="drivers_td">Lenny</td><td class="drivers_td">Tony</td><td class="drivers_td">Homz</td></tr>
		<tr class="drivers_row"><td class="drivers_td">Thursday</td><td class="drivers_td">Lenny</td><td class="drivers_td">Tony</td><td class="drivers_td">Homz</td></tr>
		<tr class="drivers_row"><td class="drivers_td">Friday</td><td class="drivers_td">Homz</td><td class="drivers_td">Tony</td><td class="drivers_td">Lenny</td></tr>
		<tr class="drivers_row"><td class="drivers_td">Saturday</td><td class="drivers_td">Homz</td><td class="drivers_td">Tony</td><td class="drivers_td">Lenny</td></tr>
		<tr class="drivers_row"><td class="drivers_td">Sunday</td><td class="drivers_td">Homz</td><td class="drivers_td">Tony</td><td class="drivers_td">Lenny</td></tr>

	</table>
	
		<p>
			If, after calling all three drivers, nobody is available, call Peter 0274 905 383.
		</p>



<h2>Local outcalls</h2>					
<table class="drivers_table">
<tr class="drivers_header"><th>No. of hours</th><th>Total cost</th><th>Driver</th></th></tr>
<tr class="drivers_row"><td class="drivers_td">1</td><td class="drivers_td">210</td><td class="drivers_td">30</td></tr>
<tr class="drivers_row"><td class="drivers_td">2</td><td class="drivers_td">410</td><td class="drivers_td">50</td></tr>
<tr class="drivers_row"><td class="drivers_td">3</td><td class="drivers_td">610</td><td class="drivers_td">70</td></tr>
<tr class="drivers_row"><td class="drivers_td">4</td><td class="drivers_td">800</td><td class="drivers_td">80</td></tr>
<tr class="drivers_row"><td class="drivers_td">5</td><td class="drivers_td">1000</td><td class="drivers_td">100</td></tr>
<tr class="drivers_row"><td class="drivers_td">6</td><td class="drivers_td">1180</td><td class="drivers_td">100</td></tr>
<tr class="drivers_row"><td class="drivers_td">7</td><td class="drivers_td">1360</td><td class="drivers_td">100</td></tr>
<tr class="drivers_row"><td class="drivers_td">8</td><td class="drivers_td">1540</td><td class="drivers_td">100</td></tr>
<tr class="drivers_row"><td class="drivers_td">9</td><td class="drivers_td">1720</td><td class="drivers_td">100</td></tr>
<tr class="drivers_row"><td class="drivers_td">10</td><td class="drivers_td">1900</td><td class="drivers_td">100</td></tr>

</table>

<h2>Long distance outcalls</h2>	
						<table class="drivers_table">

<tr class="drivers_header"> <th>Place</th><th>1 hour</th><th>2hrs</th><th>2nd Client<br />add</th><th>Extension<br />each client</th></tr>

<tr class="drivers_row"><td class="drivers_td">Airport</td><td class="drivers_td">40</td><td class="drivers_td">60</td><td class="drivers_td">20</td><td class="drivers_td">20</td></tr>
<tr class="drivers_row"><td>Arohena</td><td>90</td><td>130</td><td>40</td><td>50</td></tr>
<tr class="drivers_row"><td class="drivers_td">Bucklands Road Roger</td><td class="drivers_td">70</td><td class="drivers_td">100</td><td class="drivers_td">30</td><td class="drivers_td">40</td></tr>
<tr class="drivers_row"><td class="drivers_td">Cambridge</td><td class="drivers_td">50</td><td class="drivers_td">70</td><td class="drivers_td">20</td><td class="drivers_td">30</td></tr>
<tr class="drivers_row"><td class="drivers_td">Cussens Road Morrinsville</td><td class="drivers_td">70</td><td class="drivers_td">100</td><td class="drivers_td">30</td><td class="drivers_td">40</td></tr>
<tr class="drivers_row"><td class="drivers_td">Driver Road Horsham Downs</td><td class="drivers_td">50</td><td class="drivers_td">70</td><td class="drivers_td">20</td><td class="drivers_td">30</td></tr>
<tr class="drivers_row"><td class="drivers_td">Eureka</td><td class="drivers_td">40</td><td class="drivers_td">60</td><td class="drivers_td">20</td><td class="drivers_td">30</td></tr>
<tr class="drivers_row"><td class="drivers_td">Gordonton</td><td class="drivers_td">40</td><td class="drivers_td">60</td><td class="drivers_td">20</td><td class="drivers_td">30</td></tr>
<tr class="drivers_row"><td class="drivers_td">Horotiu</td><td class="drivers_td">40</td><td class="drivers_td">60</td><td class="drivers_td">20</td><td class="drivers_td">30</td></tr>
<tr class="drivers_row"><td class="drivers_td">Huntly</td><td class="drivers_td">60</td><td class="drivers_td">90</td><td class="drivers_td">30</td><td class="drivers_td">40</td></tr>
<tr class="drivers_row"><td class="drivers_td">Karapiro</td><td class="drivers_td">60</td><td class="drivers_td">90</td><td class="drivers_td">30</td><td class="drivers_td">40</td></tr>
<tr class="drivers_row"><td class="drivers_td">Kihikihi</td><td class="drivers_td">60</td><td class="drivers_td">90</td><td class="drivers_td">30</td><td class="drivers_td">40</td></tr>
<tr class="drivers_row"><td class="drivers_td">Matangi</td><td class="drivers_td">40</td><td class="drivers_td">60</td><td class="drivers_td">20</td><td class="drivers_td">30</td></tr>
<tr class="drivers_row"><td class="drivers_td">Morrinsville</td><td class="drivers_td">60</td><td class="drivers_td">90</td><td class="drivers_td">30</td><td class="drivers_td">40</td></tr>
<tr class="drivers_row"><td class="drivers_td">Mystery Creek</td><td class="drivers_td">40</td><td class="drivers_td">60</td><td class="drivers_td">20</td><td class="drivers_td">20</td></tr>
<tr class="drivers_row"><td class="drivers_td">Ngaruawahia</td><td class="drivers_td">40</td><td class="drivers_td">60</td><td class="drivers_td">20</td><td class="drivers_td">30</td></tr>
<tr class="drivers_row"><td class="drivers_td">Ohaupo</td><td class="drivers_td">40</td><td class="drivers_td">60</td><td class="drivers_td">20</td><td class="drivers_td">30</td></tr>
<tr class="drivers_row"><td class="drivers_td">Ohinewai</td><td class="drivers_td">60</td><td class="drivers_td">90</td><td class="drivers_td">30</td><td class="drivers_td">40</td></tr>
<tr class="drivers_row"><td class="drivers_td">Okoroire</td><td class="drivers_td">80</td><td class="drivers_td">120</td><td class="drivers_td">40</td><td class="drivers_td">50</td></tr>
<tr class="drivers_row"><td class="drivers_td">Otorohanga</td><td class="drivers_td">90</td><td class="drivers_td">130</td><td class="drivers_td">40</td><td class="drivers_td">50</td></tr>
<tr class="drivers_row"><td class="drivers_td">Paeroa</td><td class="drivers_td">100</td><td class="drivers_td">140</td><td class="drivers_td">40</td><td class="drivers_td">50</td></tr>
<tr class="drivers_row"><td class="drivers_td">Pirongia</td><td class="drivers_td">50</td><td class="drivers_td">70</td><td class="drivers_td">20</td><td class="drivers_td">30</td></tr>
<tr class="drivers_row"><td class="drivers_td">Pukemiro</td><td class="drivers_td">80</td><td class="drivers_td">120</td><td class="drivers_td">40</td><td class="drivers_td">50</td></tr>
<tr class="drivers_row"><td class="drivers_td">Puketaha</td><td class="drivers_td">70</td><td class="drivers_td">100</td><td class="drivers_td">30</td><td class="drivers_td">40</td></tr>
<tr class="drivers_row"><td class="drivers_td">Putaruru</td><td class="drivers_td">90</td><td class="drivers_td">130</td><td class="drivers_td">40</td><td class="drivers_td">50</td></tr>
<tr class="drivers_row"><td class="drivers_td">Raglan</td><td class="drivers_td">80</td><td class="drivers_td">120</td><td class="drivers_td">40</td><td class="drivers_td">50</td></tr>
<tr class="drivers_row"><td class="drivers_td">Rangiriri</td><td class="drivers_td">70</td><td class="drivers_td">100</td><td class="drivers_td">30</td><td class="drivers_td">40</td></tr>
<tr class="drivers_row"><td class="drivers_td">Springdale</td><td class="drivers_td">80</td><td class="drivers_td">120</td><td class="drivers_td">40</td><td class="drivers_td">50</td></tr>
<tr class="drivers_row"><td class="drivers_td">Tahuna</td><td class="drivers_td">70</td><td class="drivers_td">100</td><td class="drivers_td">30</td><td class="drivers_td">40</td></tr>
<tr class="drivers_row"><td class="drivers_td">Tamahere</td><td class="drivers_td">40</td><td class="drivers_td">60</td><td class="drivers_td">20</td><td class="drivers_td">30</td></tr>
<tr class="drivers_row"><td class="drivers_td">Taupiri</td><td class="drivers_td">50</td><td class="drivers_td">70</td><td class="drivers_td">20</td><td class="drivers_td">30</td></tr>
<tr class="drivers_row"><td class="drivers_td">Te Akau South Road</td><td class="drivers_td">100</td><td class="drivers_td">140</td><td class="drivers_td">40</td><td class="drivers_td">50</td></tr>
<tr class="drivers_row"><td class="drivers_td">Te Aroha</td><td class="drivers_td">80</td><td class="drivers_td">120</td><td class="drivers_td">40</td><td class="drivers_td">50</td></tr>
<tr class="drivers_row"><td class="drivers_td">Te Awamutu</td><td class="drivers_td">50</td><td class="drivers_td">70</td><td class="drivers_td">20</td><td class="drivers_td">30</td></tr>
<tr class="drivers_row"><td class="drivers_td">Te Kauwhata</td><td class="drivers_td">80</td><td class="drivers_td">120</td><td class="drivers_td">40</td><td class="drivers_td">50</td></tr>
<tr class="drivers_row"><td class="drivers_td">Te Pahu</td><td class="drivers_td">60</td><td class="drivers_td">90</td><td class="drivers_td">30</td><td class="drivers_td">40</td></tr>
<tr class="drivers_row"><td class="drivers_td">Tirau</td><td class="drivers_td">80</td><td class="drivers_td">120</td><td class="drivers_td">40</td><td class="drivers_td">50</td></tr>
<tr class="drivers_row"><td class="drivers_td">Waharoa</td><td class="drivers_td">90</td><td class="drivers_td">120</td><td class="drivers_td">40</td><td class="drivers_td">50</td></tr>
<tr class="drivers_row"><td class="drivers_td">Waihou</td><td class="drivers_td">70</td><td class="drivers_td">100</td><td class="drivers_td">30</td><td class="drivers_td">40</td></tr>
<tr class="drivers_row"><td class="drivers_td">Waitoa</td><td class="drivers_td">70</td><td class="drivers_td">100</td><td class="drivers_td">30</td><td class="drivers_td">40</td></tr>
<tr class="drivers_row"><td class="drivers_td">Walton</td><td class="drivers_td">90</td><td class="drivers_td">130</td><td class="drivers_td">40</td><td class="drivers_td">50</td></tr>
<tr class="drivers_row"><td class="drivers_td">Whatawhata</td><td class="drivers_td">40</td><td class="drivers_td">60</td><td class="drivers_td">20</td><td class="drivers_td">30</td></tr>

</table>

						
						<?php
						echo '</td></tr></table>';
					?>


	<?php
		write_credits();
	?>

</body>
</html>