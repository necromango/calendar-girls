<?php
require_once('includes/db_worx.php');
require_once('includes/cgdefs.php');
require_once('includes/cgpicops.php');
require_once('includes/job_item_ops.php');

function write_log($log){
	
	date_default_timezone_set('Pacific/Auckland');
	$entry 	=  date('Ymd H:i:s') . 
	$entry = $entry . ' ' . $log . '
';

	file_put_contents(cal_log, $entry, FILE_APPEND);
}

function get_pix(){

	$query = 'select * FROM pix';			
	$result = wcallq($query);
	
	while($result_row = mysql_fetch_array($result, MYSQL_ASSOC)) {	
		
		$pix[$result_row['name']]['name']		=	$result_row['name'];
		$pix[$result_row['name']]['rel_url']	=	$result_row['rel_url'];
		$pix[$result_row['name']]['width']		=  	$result_row['width'];
		$pix[$result_row['name']]['height']		=	$result_row['height'];
		$pix[$result_row['name']]['alt']		= 	$result_row['alt'];
		$pix[$result_row['name']]['caption']	= 	$result_row['caption'];
		$pix[$result_row['name']]['cat_1']		= 	$result_row['cat_1'];
		$pix[$result_row['name']]['cat_2']		= 	$result_row['cat_2'];
		$pix[$result_row['name']]['comment']	= 	$result_row['comment'];
		$pix[$result_row['name']]['date']		= 	$result_row['date'];

	}		
	return $pix;
}

function write_pic($pic) {
	echo '<img width = "' . $pic['width'] . '" height = "' . $pic['height'] . '" src = "' . $pic['rel_url'] . '" alt = "' . $pic['alt'] . '">';	
}

function buildEditForm ($new, $girl_id){
	
	$gp = get_gp($girl_id);
	echo '<!-- start function buildEditForm ($new, $girl_id) -->
		
	';
	if ($new == 2){
		//	if $new is set to '2' that means we are editing an existing record
		
		//	We will use $res later to decide whether or
		//	not to hide the existing values in the form so that we can access them
		//	when we process the form
		$content = '<h2>Edit details here for : ' . $gp['name'] . '</h2>'
		. $gp['name'] . '\'s id no. is : ' . $gp['girl_id'] . '<br /><br />
	
	';
		$res = 1;
	}
	

	//	start putting the content to be written into $form
	//	use the $fld[] array to put the existing values into the fields of the form
	//	since $res has a value we will hide the existing values in hidden fields at the 
	//	end of the form and then retrieve them when we process the form and compare them 
	//	to what was put into the form. This way we know what needs to be updated and can
	//	take action accordingly
	//	There is also a hidden field 'editform' We check for this back on edit_who.php to 
	//	see whether or not the form has been submitted.
	
	$fld[0] = $gp['girl_id'];
	$fld[1] = $gp['name'];
	$fld[2] = $gp['age'];
	$fld[3] = $gp['height'];
	$fld[4] = $gp['size'];
	$fld[5] = $gp['hair'];
	$fld[6] = $gp['eyes'];
	$fld[7] = $gp['bust'];
	$fld[8] = $gp['oc'];
	$fld[9] = $gp['comments'];
	$fld[10]= $gp['girl_id'];
	
	
	$form 	= 	'<form action = "';
	$form 	.=	$_SERVER['PHP_SELF'];
	$form 	.= 	'" method = "GET">
	
	<table border = "0">
		
		<tr>
			<td>
				<label>Name
			</td>
			<td>
				<input type = "text" name = "name" value = "' . $gp['name'] . '" /></label><div class = "right_txt"><a href="' . $_SERVER['PHP_SELF'] . '"><img src = "pix/nav/go.png"></a></div></td>
		</tr>
		<tr>
			<td>
				<label>Age</td><td><input type = "text" name = "age" value = "' . $gp['age'] . '" /></label>
			</td>
		</tr>	
		<tr>
			<td>
				<label>Height</td><td><input type = "text" name = "height" value = "' . $gp['height'] . '" /></label>
			</td>
		</tr>	
		<tr>
			<td>
				<label>Size</td><td><input type = "text" name = "size" value = "' . $gp['size'] . '" /></label>
			</td>
		</tr>	
		<tr>
			<td>
				<label>Hair</td><td><input type = "text" name = "hair" value = "' . $gp['hair'] . '" /></label>
			</td>
		</tr>	
		<tr>
			<td>
				<label>Eyes</td><td><input type = "text" name = "eyes" value = "' . $gp['eyes'] . '" /></label>
			</td>
		</tr>	
		<tr>
			<td>
				<label>Bust</td><td><input type = "text" name = "bust" value = "' . $gp['bust'] . '" /></label>
			</td>
		</tr>	
		<tr>
			<td>
				<label>Outcalls</td><td><input type = "text" name = "outcalls" value = "' . $gp['oc'] . '" /></label>
			</td>
		</tr>	
		<tr>
			<td>
				<label>Comments</td><td><textarea rows = "5" cols = "40" name = "comments">' . $gp['comments'] .'</textarea></label>
			</td>
		</tr>	
		<tr>
			<td>
				<input type = "hidden" name = "editform" value = "1" />' . ($res ? '<input type = "hidden" name = "fld1" value = "' . $fld[1] . '" /><input type = "hidden" name = "fld2" value = "' . $fld[2] . '" /><input type = "hidden" name = "fld3" value = "' . $fld[3] . '" /><input type = "hidden" name = "fld4" value = "' . $fld[4] . '" /><input type = "hidden" name = "fld5" value = "' . $fld[5] . '" /><input type = "hidden" name = "fld6" value = "' . $fld[6] . '" /><input type = "hidden" name = "fld7" value = "' . $fld[7] . '" /><input type = "hidden" name = "fld8" value = "' . $fld[8] . '" /><input type = "hidden" name = "fld9" value = "' . $fld[9] . '" /><input type = "hidden" name = "fld10" value = "' . $fld[10] . '" />' : '') . '</td><td align = "right"><input type = "submit" value = "Go" />
			</td>
		</tr>			
	
	</table>
	
	</form>';



			
	//	write the content
	echo $content;
	//	write the form
	echo $form;
	
	echo '<p>Click <a href="' . $_SERVER['PHP_SELF'] . '?id=del_g' . $fld[0] . '">HERE TO DELETE</a> the entire record for ' . $fld[1] . '
	<br />
	DO NOT do this unless you are sure it is what you want to do
	<br />
	It can\'t be undone</p>
	
	<p><a href="picshop.php?id=' . $fld[0] . '">View images</a></p>	<p>Preview <a href="profile.php?id=preview' . $fld[0] . '">' . $gp['name'] . '\'s web page</a></p>
<!-- end function buildEditForm ($new, $girl_id) -->

';

}								//	format corrected

function buildViewGirl($gp, $location) {

	$table = '';
	
	if ($location == 'edit') {
		$location = 'edit_who';
		$table = '<table><tr><td class = "xt"><a href="' . $_SERVER['PHP_SELF'] . '?id=del_g' . $gp['girl_id'] . '"><img src = "pix/nav/close.png"></a></td><td class = "xt"><a href="' . $_SERVER['PHP_SELF'] . '?id=signon_h' . $gp['girl_id'] . '">sign on htn</a></td><td class = "xt"><a href="' . $_SERVER['PHP_SELF'] . '?id=signon_t' . $gp['girl_id'] . '">sign on tga</a></td><td class = "xt"><a href="' . $_SERVER['PHP_SELF'] . '?id=ed_g' . $gp['girl_id'] . '"><img src = "pix/nav/lock.jpg"></a></td></tr></table>';
	}

	$table .= '<table border = "0" class = "med1text">
	';
	$table .= '<tr>
		<td class = "xt">
		</td>
		<td class = "xtx">' . $gp['name'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "xt">Age
		</td>
		<td class = "xt">' . $gp['age'] . '</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "xt">Height
		</td>
		<td class = "xt">' . $gp['height'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "xt">Size
		</td>
		<td class = "xt">' . $gp['size'] . '
		</td>	
	</tr>
	';
	$table .= '<tr>
		<td class = "xt">Hair
		</td>
		<td class = "xt">' . $gp['hair'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "xt">Eyes
		</td>
		<td class = "xt">' . $gp['eyes'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "xt">Bust
		</td>
		<td class = "xt">' . $gp['bust'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "xt">Outcalls
		</td>
		<td class = "xt">' . $gp['oc'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "xt">
		</td>
		<td class = "xt"><hr>' . $gp['comments'] . '
		</td>
	</tr>
	';
	$table .= '
</table>

';

	echo $table;
}								//	format corrected

function upd_girls_from_form() {

	//	set $chg to 0 - we'll test for it later
	$chg = 0;	
	//	init $result
	$result = 0;
	
	//	$_GET['fld1'] will only have a value if we retrieved the existing values from the record
	//	this means we are modifying a record rather than creating a new one
	//	since it does have a value we now move on to test which fields in the form were modified
	if($_GET['fld1']) {

		//	if the retrieved value hidden in the form does not match what came from the form itself
		//	we know it must have been updated so we build a query to do just that
		if($_GET['fld1'] != $_GET['name']) {

			//	assign the new field value to $name			
			$name 	= $_GET['name'];

			//	$_GET['fld10'] is the primary key 'girl_ID' from the database			
			$query = 'UPDATE girls SET name = "' . $name . '" WHERE girl_ID = ' . $_GET['fld10'] . ';';

			
			$result = wwopq($query);
			if (!$result) {
				die ('Could not query the database : <br /> ' . mysql_error());
			}
			
		//	give $chg a value since something has been updated
		$chg = 1;
		
		}
		
		//	same for all the other fields
		
		if($_GET['fld2'] != $_GET['age']) {
			$age 	= $_GET['age'];
				$query = 'UPDATE girls SET age = "' . $age . '" WHERE girl_ID = ' . $_GET['fld10'] . ';';
				$result = wwopq($query);
				if (!$result) {
					die ('Could not query the database : <br /> ' . mysql_error());
				}
			$chg = 1;
		}
		if ($_GET['fld3'] != $_GET['height']) {
			$height = $_GET['height'];
				$query = 'UPDATE girls SET height = "' . $height . '" WHERE girl_ID = ' . $_GET['fld10'] . ';';
				$result = wwopq($query);
				if (!$result) {
					die ('Could not query the database : <br /> ' . mysql_error());
				}
			$chg = 1;
		}
		if ($_GET['fld4'] != $_GET['size']) {
			$size 	= $_GET['size'];
				$query = 'UPDATE girls SET size = "' . $size . '" WHERE girl_ID = ' . $_GET['fld10'] . ';';
				$result = wwopq($query);
				if (!$result) {
					die ('Could not query the database : <br /> ' . mysql_error());
				}
			$chg = 1;
		}
		if ($_GET['fld5'] != $_GET['hair']) {
			$hair 	= $_GET['hair'];
				$query = 'UPDATE girls SET hair = "' . $hair . '" WHERE girl_ID = ' . $_GET['fld10'] . ';';
				$result = wwopq($query);
				if (!$result) {
					die ('Could not query the database : <br /> ' . mysql_error());
				}
			$chg = 1;
		}
		if ($_GET['fld6'] != $_GET['eyes']) {
			$eyes 	= $_GET['eyes'];
				$query = 'UPDATE girls SET eyes = "' . $eyes . '" WHERE girl_ID = ' . $_GET['fld10'] . ';';
				$result = wwopq($query);
				if (!$result) {
					die ('Could not query the database : <br /> ' . mysql_error());
				}
			$chg = 1;
		}
		if ($_GET['fld7'] != $_GET['bust']) {
			$bust 	= $_GET['bust'];
				$query = 'UPDATE girls SET bust = "' . $bust . '" WHERE girl_ID = ' . $_GET['fld10'] . ';';
				$result = wwopq($query);
				if (!$result) {
					die ('Could not query the database : <br /> ' . mysql_error());
				}
			$chg = 1;
		}
		if ($_GET['fld8'] != $_GET['outcalls']) {
			$outcalls = $_GET['outcalls'];
				$query = 'UPDATE girls SET oc = "' . $outcalls . '" WHERE girl_ID = ' . $_GET['fld10'] . ';';
				$result = wwopq($query);
				if (!$result) {
					die ('Could not query the database : <br /> ' . mysql_error());
				}
			$chg = 1;
		}
		if ($_GET['fld9'] != $_GET['comments']) {
			$comments = $_GET['comments'];
				$query = 'UPDATE girls SET comments = "' . $comments . '" WHERE girl_ID = ' . $_GET['fld10'] . ';';
				$result = wwopq($query);
				if (!$result) {
					die ('Could not query the database : <br /> ' . mysql_error());
				}
			$chg = 1;
		}
		
		//	If there have been any changes - act accordingly
		if($chg) {
			echo '<p>You have successfully updated the records.</p>';
			echo '<p><a href="' . $SERVER['PHP_SELF'] . '?id="><img src = "pix/nav/go.png"></a></p>';
			
		//	if nothing was changed ...	
		} else {
			echo '<p>Nothing was changed.</p>';
			echo '<p><a href="' . $SERVER['PHP_SELF'] . '?id="><img src = "pix/nav/go.png"></a></p>';
		}

	} 
	
	//	if we are not modifying a record we must be creating a new one ...
	//	no need to retrieve any existing values
	else {
		
		$name 		= $_GET['name'];
		$age 		= $_GET['age'];
		$height 	= $_GET['height'];
		$size 		= $_GET['size'];
		$hair 		= $_GET['hair'];
		$eyes 		= $_GET['eyes'];
		$bust 		= $_GET['bust'];
		$outcalls 	= $_GET['outcalls'];
		$comments 	= $_GET['comments'];
			
		echo '<p>Let\'s make a new record<br />$_GET[\'name\'] is : ' . $_GET['name'] . '</p>';
		
		$query = "INSERT INTO `cgirls`.`girls` (`girl_ID`, `on_off`, `name`, `age`, `height`, `size`, `hair`, `eyes`, `bust`, `oc`, `comments`) VALUES (NULL, '', '$name', '$age', '$height', '$size', '$hair', '$eyes', '$bust', '$outcalls', '$comments')";

		$result = wwopq($query);
		if (!$result) {
			die ('Could not query to the database : <br /> ' . mysql_error());
		}
		
		//	haven't checked here that anything has actually been entered
		//	should do that
		echo '<p>You have successfully added a new record.</p>';
		echo '<p><a href="' . $SERVER['PHP_SELF'] . '?id="><img src = "pix/nav/go.png"></a></p>';
		
		//	Now we must create an image directory for the girl
		//	For now I will put a link to do this on her details page

		
	}

}

function sign_on($loc) {
	
	$_GET["id"] = substr($_GET["id"],8);
	if ($loc == 1) {	
		$query = 'UPDATE girls SET on_off = "1" WHERE girl_id = "' . $_GET["id"] . '"';
	} elseif ($loc == 2) {	
		$query = 'UPDATE girls SET on_off = "2" WHERE girl_id = "' . $_GET["id"] . '"';
	}
		$result = wwopq($query);
	if (!$result) {
		die ('Could not query the database : <br /> ' . mysql_error());
	}
}

function delete_rec() {
	$_GET["id"] = substr($_GET["id"],5);
					
	$query = 'DELETE FROM girls WHERE girl_id = "' . $_GET["id"] . '"';			
	$result = wwopq($query);
	
	echo '<p>The record for id number ' . $_GET["id"] . ' has been deleted.</p><p><a href = "' . $_SERVER['PHP_SELF'] . '?id="><img src = "pix/nav/go.png"></a></p>';
}

function sign_off() {
	$_GET["id"] = substr($_GET["id"],7);			
	$query = 'UPDATE girls SET on_off = "0" WHERE girl_id = "' . $_GET["id"] . '"';	
	$result = wwopq($query);
	if (!$result) {
		die ('Could not query the database : <br /> ' . mysql_error());
	}

}

function writePst($pstype, $gp) {
	// This is to write the style section for the photostrip and rollovers

	if ($pstype == 'pro') {
		$style = '<style type="text/css">
<!--
';	
		for ($j = 1; $j < 11; $j++) {
			$x = 'fs' . $j;
			if ($gp[$x] == '') {
				// echo 'Outta here : $gp[$x] is : ' . $gp[$x] . '$j is : ' . $j . '<br />';
				$style .= '#pst' . $j . '{ background-image : url();}
			';
			}	
			else {
			$style .= '#pst' . $j . '{ background-image : url(' . $gp[$x] . 'tn.jpg);}
			';
			}
	}	
	$style .= '-->
</style>
';
	echo $style;
	return $j - 1;
	}
	elseif ($pstype == 'gen') {
		echo '<style type="text/css">
<!--
#pst1{ background-image : url(' . PST_1_ST . 'tn.jpg); }
#pst2{ background-image : url(' . PST_2_ST . 'tn.jpg); }
#pst3{ background-image : url(' . PST_3_ST . 'tn.jpg); }
#pst4{ background-image : url(' . PST_4_ST . 'tn.jpg); }
#pst5{ background-image : url(' . PST_5_ST . 'tn.jpg); }
#pst6{ background-image : url(' . PST_6_ST . 'tn.jpg); }
#pst7{ background-image : url(' . PST_7_ST . 'tn.jpg); }
#pst8{ background-image : url(' . PST_8_ST . 'tn.jpg); }
#pst9{ background-image : url(' . PST_9_ST . 'tn.jpg); }
#pst10{ background-image : url(' . PST_10_ST . 'tn.jpg); }

-->
</style>';
	}
	elseif ($pstype == 'htnrooms'){
		echo '<style type="text/css">
<!--
#pst1{ background-image : url(pix/rooms/IMG_2398tn.JPG); }
#pst2{ background-image : url(pix/rooms/IMG_2406tn.jpg); }
#pst3{ background-image : url(pix/rooms/IMG_2434tn.jpg); }
#pst4{ background-image : url(pix/rooms/IMG_2435tn.jpg); }
#pst5{ background-image : url(pix/rooms/IMG_2436tn.jpg); }
#pst6{ background-image : url(pix/rooms/IMG_2437tn.jpg); }
#pst7{ background-image : url(pix/rooms/IMG_2439tn.jpg); }
#pst8{ background-image : url(pix/rooms/IMG_2441tn.jpg); }
#pst9{ background-image : url(pix/rooms/IMG_2445tn.jpg); }
#pst10{ background-image : url(pix/rooms/IMG_2446tn.jpg); }
-->
</style>';
	}
	elseif ($pstype == 'ren'){
		echo '<style type="text/css">
<!--
#pst1{ background-image : url(pix/renov/IMG_2223tn.JPG); }
#pst2{ background-image : url(pix/renov/IMG_2227tn.JPG); }
#pst3{ background-image : url(pix/renov/IMG_2229tn.JPG); }
#pst4{ background-image : url(pix/renov/IMG_2238tn.JPG); }
#pst5{ background-image : url(pix/renov/IMG_2247tn.JPG); }
#pst6{ background-image : url(pix/renov/IMG_2252tn.JPG); }
#pst7{ background-image : url(pix/renov/IMG_2278tn.JPG); }
#pst8{ background-image : url(pix/renov/IMG_2288tn.JPG); }
#pst9{ background-image : url(pix/renov/IMG_2294tn.JPG); }
#pst10{ background-image : url(pix/renov/IMG_2314tn.JPG); }
-->
</style>';
	}
}

function get_gp($girl_id){

	$query = 'select * FROM girls WHERE girl_id = "' . $girl_id . '"';			
	$result = wcallq($query);
	
	while($result_row = mysql_fetch_array($result, MYSQL_ASSOC)) {	
		
		$gp['girl_id']	=	$result_row['girl_ID'];
		$gp['home']		=	$result_row['home'];
		$gp['name']		=  	$result_row['name'];
		$gp['age']		=	$result_row['age'];
		$gp['height']	= 	$result_row['height'];
		$gp['size']		= 	$result_row['size'];
		$gp['hair']		= 	$result_row['hair'];
		$gp['eyes']		= 	$result_row['eyes'];
		$gp['bust']		= 	$result_row['bust'];
		$gp['oc']		= 	$result_row['oc'];
		$gp['comments']	= 	$result_row['comments'];
		$gp['has_pix']		=	$result_row['has_pix'];
		$gp['fs1']		=	$result_row['fs1'];
		$gp['fs1_int']		=	$result_row['fs1_int'];
		$gp['fs2']		=	$result_row['fs2'];
		$gp['fs2_int']		=	$result_row['fs2_int'];
		$gp['fs3']		=	$result_row['fs3'];
		$gp['fs3_int']		=	$result_row['fs3_int'];
		$gp['fs4']		=	$result_row['fs4'];
		$gp['fs4_int']		=	$result_row['fs4_int'];
		$gp['fs5']		=	$result_row['fs5'];
		$gp['fs5_int']		=	$result_row['fs5_int'];
		$gp['fs6']		=	$result_row['fs6'];
		$gp['fs6_int']		=	$result_row['fs6_int'];
		$gp['fs7']		=	$result_row['fs7'];
		$gp['fs7_int']		=	$result_row['fs7_int'];
		$gp['fs8']		=	$result_row['fs8'];
		$gp['fs8_int']		=	$result_row['fs8_int'];
		$gp['fs9']		=	$result_row['fs9'];
		$gp['fs9_int']		=	$result_row['fs9_int'];
		$gp['fs10']		=	$result_row['fs10'];
		$gp['fs10_int']		=	$result_row['fs10_int'];
		
	}		
	return $gp;
}
	
function on_shift_list($location) {
	
	//	$location is passed to the function from the calling page
	//	It tells us which location we are dealing with
	
	//	$location is completely the wrong name for this variable - it must be changed.
	
	if ($location != 'edit'){
		
		//	build the who's on box
		date_default_timezone_set('Pacific/Auckland');
		echo '
<!-- start function on_shift_list($location) -->
	';
		$box	= '';
		$box 	.= '<div id ="date">
		' . date('D jS \ F h:i A') . '
		<hr>
	</div>
	';
		$box 	.= '<div id = "wo">
		';
		$box	.= '<div class = "medtext">
			<marquee behavior="alternate" direction="left" scrollamount="1" loop="true" width="100%">Available now</MARQUEE>
		</div>
		';
		
		//	Hamilton

		$query = 'SELECT name, girl_id FROM girls WHERE on_off = 1';
		$result = wcallq($query);
		if ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
			$result = wcallq($query);
			$box	.= '<div class = "wotext">
			<!--<br />
			Hamilton-->
		</div>
		';
			$box	.= '<div class = "shiftlist">
			';
			while($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
				$box	.= '<a href = "profile.php?id=1' . $row['girl_id'] . '">' . $row['name'] . '</a>
			<br />';
			}
			$box	.= '
		</div>
	';
		}
		
		//	Frankton

		$query = 'SELECT name, girl_id FROM girls WHERE on_off = 3';
		$result = wcallq($query);
		if ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
			$result = wcallq($query);
			$box	.= '<div class = "wotext">
			<br />
			Frankton
		</div>
		';
			$box	.= '<div class = "shiftlist">
			';
			while($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
				$box	.= '<a href = "profile.php?id=1' . $row['girl_id'] . '">' . $row['name'] . '</a>
			<br />';
			}
			$box	.= '
		</div>
	';
		}
		
		
		//	Tauranga
		$query = 'SELECT name, girl_id FROM girls WHERE on_off = 2';
		$result = wcallq($query);
		if ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
			$result = wcallq($query);
			$box	.= '	<div class = "wotext">
			<br />
			Tauranga
		</div>
		';
			
			$box	.= '<div class = "shiftlist">
			';
			while($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
				$box	.= '<a href = "profile.php?id=2' . $row['girl_id'] . '">' . $row['name'] . '</a>
			<br />';
			}
			$box	.= '
		</div>
	';
		}
		
		$box	.= '</div>';
		//	write the box
		echo $box;

	} else {
		
		//	$location must be 'edit' which means we are serving up 'edit_who.php'
		echo'<div><h2>On now in Hamilton</h2>';
		//	define query for Hamilton
		$query = 'SELECT name, girl_id FROM girls WHERE on_off = 1';
		
		$result = wcallq($query);
		echo '<table border = "0" cellpadding = "3">';
		while($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
			echo '<tr><td>' . $row['name'] . '</td><td class = "xt"><a href="' . $_SERVER['PHP_SELF'] . '?id=signoff' . $row['girl_id'] . '"><img src = "pix/nav/drop.png"></a></td></tr>';
	
		}
		echo '</table>';
		
		
		/*echo'<h2>On now in Tauranga</h2>';
		//	define query for Tauranga
		$query = 'SELECT name, girl_id FROM girls WHERE on_off = 2';

		$result = wcallq($query);
		echo '<table border = "0" cellpadding = "3">';
		while($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
			echo '<tr><td>' . $row['name'] . '</td><td class = "xt"><a href="' . $_SERVER['PHP_SELF'] . '?id=signoff' . $row['girl_id'] . '"><img src = "pix/nav/drop.png"></a></td></tr>';
	
		}
		echo '</table>
		*/
		echo '</div>';
	}
	echo '
<!-- end function on_shift_list($location) -->
		
';
}										//	format corrected

function writePromoPic($girl_id) {

	$query = 'SELECT name, has_pix FROM girls WHERE girl_id = "' . $girl_id . '"';
	$result = wcallq($query);
	while($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
		$name	= $row['name'];
		$has_pix	= $row['has_pix'];
	}
	$prpic = '
	<!-- start function writePromoPic() -->
	';
	
	if(stristr($_SERVER['PHP_SELF'], 'profile.php')) {
		$prpic	.= '<div id = "top_left_box">';
		if ($has_pix) {
			$prpic	.=	'	Click on the photo strip
			';
			$prpic .= '<center><img src = "pix/prof/' .  $girl_id . '/' . $girl_id . '100.jpg" width = "' . $gp['main_pic_width'] . '" height = "' . $gp['main_pic_height'] . '" name = "promopic"></center>
			';
		}
		else{
			$prpic .= '	<p>' . $name . ' does not have any photos yet.</p>
			';
			
		}
			$prpic .=	'
		</div>';
	}
	else{
		$prpic	.= '<div id = "top_left_box">
		';
		$query = 'SELECT name FROM girls WHERE girl_id = "' . PROMO_PIC . '"';
		$result = wcallq($query);
		while($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
			$name	= $row['name'];
		}
		$prpic	.=	'Click below for more of ' . $name;
		$prpic 	.=	'<a href="profile.php?id=1' . PROMO_PIC . '"><img src ="pix/prof/' . PROMO_PIC . '/' . PROMO_PIC . '100.jpg" name = "promopic"></a>
		';
		$prpic .=	'
	</div>
	';
		$prpic .=	'<!-- end function writePromoPic() -->
		';
	
	}
	echo $prpic;

}

function sign_on_off_list() {
	
	$query = 'SELECT  name, girl_id FROM girls ORDER BY name';
	$result = wcallq($query);
	
	$f = 1;
	while($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
		$tab[$f]['name'] = $row['name'];
		$tab[$f]['id'] = $row['girl_id'];
		$f++;
	}
	
	$tablex = '';
	$tablex .=	'<table border = "1" width = "100%">
	';
	
	$d = 1;
	for ($i = 1; $i < ($f + 7)/4; $i++){
	
		$tablex .=	'	<tr>';
		
		for ($j = 1; $j < 5; $j++){
			$tablex .=	'
			<td class = "ladiesList"><a href="' . $_SERVER['PHP_SELF'] . '?id=ed_g' . $tab[$d]['id'] . '">' . $tab[$d]['name'] . '</a></td>
			<td class = "ladiesList"><a href="' . $_SERVER['PHP_SELF'] . '?id=signon_h' . $tab[$d]['id'] . '">H</a></td>

			<!--<td class = "ladiesList"><a href="' . $_SERVER['PHP_SELF'] . '?id=signon_t' . $tab[$d]['id'] . '">T</a></td>-->

		';
			$d++;
		}
		$tablex .=	'</tr>
	';
	}
$tablex .=	'</table>';
	
	echo $tablex;

}

function write_credits() {
	
	echo '<!-- start function write_credits() -->
	<table border = "0">
		<tr>
			<td>
				<!--<div class="credits">&copy; 2009 - 2011 Orchestral Holdings Ltd  PO Box 29 Cambridge New Zealand.</div>-->
				<img src="pix/topimg.gif" width="895" height="133">	
			</td>
		</tr>
	</table>
<!-- end function write_credits() -->

';
}												//	format corrected

function listLadies($location) {   // This does not need $location

	
	$query = 'SELECT  name, girl_id, has_pix FROM girls ORDER BY name';
	$result = wcallq($query);
	
	$f = 1;
	while($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
		//if($row['has_pix']){
			$tab[$f]['name'] = $row['name'];
			$tab[$f]['id'] = $row['girl_id'];
			$tab[$f]['has_pix'] = $row['has_pix'];
			$f++;
		//}
	}
	
	$tablex = '';
	$tablex .=	'<table width = "100%">
	';
	
	$d = 1;
	for ($i = 1; $i < ($f + 7)/4; $i++){
	
		$tablex .=	'<tr>';
		
		for ($j = 1; $j < 5; $j++){
			$tablex .=	'<td class = "ladiesList"><a href="profile.php?id=1' . $tab[$d]['id'] . '">' . $tab[$d]['name'];
			if ($tab[$d]['has_pix']){
				$tablex .=	 '*';
			}
			$tablex .=	 '</a></td>';
			$d++;
		}
		$tablex .=	'</tr>';
	}
	$tablex .=	'</table>';
	
	echo $tablex;

}

function bottomnavStrip($location){
	$ns = '';
	$ns .= '<!-- start function bottomnavStrip($location) -->
	';
	$ns .= '<div id = "bottomnavStrip">
		';
	

	$ns .= '</div>
	';
	$ns .= '<!-- end function bottomnavStrip($location) -->';
	echo $ns;
	
}										//	format corrected

function topnavStrip($location) {
	$ns = '';
	$ns .= '<!-- start function bottomnavStrip($location) -->
	';
	$ns .= '<div id = "topnavStrip">
		';
	
	if ($location == "Hamilton") {
		

			$ns .= '<a href="ladies.php">Ladies</a>
			&nbsp;|&nbsp;
			';
		
		$ns .= '<a href="loc.php">Location</a>
		&nbsp;|&nbsp;
		';
		$ns .= '<a href="htnrooms.php">Premises</a>
		&nbsp;|&nbsp;
		';
		// taken out sands link until I can work out what to put in here
		//	$ns .= '<a href="sands.php">Spa & Sauna</a>
		//&nbsp;|&nbsp;
		//';
		$ns .= '<a href="prandinfo.php">Rates</a>
		&nbsp;|&nbsp;
		';
		$ns .= '<a href="opportunities.php">Opportunities</a>

		';
		
	}
	
	$ns .= '</div>';
	$ns .= '<!-- start function bottomnavStrip($location) -->
	';
	echo $ns;
	
}										//	format corrected

function phone() {
	echo '
	<!-- start function phone() -->
	<div id = "phone">
		<span class = "smbrk"><br /></span>Hamilton 07 834 1247<span class = "smbrk"><br /><br />

		</span><!--Tauranga 07 579 0091-->
	</div>
	<!-- end function phone() -->
	';
			}	

function write_ad() {
	$num = rand(1, 52);
	
	echo '
	<!-- start function write_ad() -->
	<div class = "adbox">
		<img src = "pix/come/' . $num . '.gif"width = "172" height = "257" name = "adpic">
	</div>
	
	';
	/*
	echo '<p class = "medtext">New live web cams</p>
		<a target="_blank" href="http://www.cgtv.co.nz"><img class="cgtv-img" src = "pix/cgtv_2.png" width = "100" height = "40" alt = "cgtv"></a>
		<!-- end function write_ad() -->';
	*/
}	

function write_nav_box($location) {
	$nb = '';
	$nb .= '<div id = "nav_box">
		<ul>';
	if ($location == 'Hamilton') {
		$nb .= '		<li><a href="pageone.php">Home</a>';
		$nb .= '		<li><a href="ladies.php">Ladies</a>';
		$nb .= '		<li><a href="htnrooms.php">Premises</a>';
		$nb .= '		<li><a href="sands.php">Spa & Sauna</a>';
		$nb .= '		<li><a href="htnrenov.php">Renovations</a>';
		$nb .= '		<li><a href="prandinfo.php">Rates</a>';
		$nb .= '		<li><a href="opportunities.php">Opportunities</a>';
	}
	else {
	
	}
		$nb .= '	</ul>
		</div>';
	
	echo $nb;
}

function photoStrip($location, $pstype, $girl_id, $gp, $pic_num, $preview) {
echo '
<!-- start photostrip -->
';

	if($location == 'picshop') {
		$picshop = true;
		$preview = true;
	}
	//	Must work out here how to decide whether or not to draw the photo strip cell. No point drawing it if there is no picture
	//	Also re-comment this block of code because things have changed since it was done - and all the rest.
	
	//	This draws the photostrip at the top of the page.
	//	The thumbnails are set as the background image of each table cell by the style section.
	//	All that is decided here is where to link to from each cell, and the picture to link to
	//	which is actually the full version of the thumbnail
	//	The url of the big picture is contained in $gp[$x] which is passed to the profile page
	// in the "id" along with the girl's id.
	
	if ($pstype == 'pro') {
		$pst = '<div class = "fs">';
		
		if($picshop) {
			echo '<p>This is how the photostrip will look on ' . $gp['name'] . '\'s profile page.</p>';
		}
		
		
		$pst .= '<table border = "1" id = "pst">
			<tr>';
				// not sure how or why we need $pic_num but I'm not game to ditch it just yet
				for($i = 1; $i <= $pic_num; $i++) {
					$x = 'fs' . $i;
					$approval = $x . '_int';
					if($gp[$x] != '' && ($gp[$x . '_int'] == 3 || $preview)){
						$pst .= '
						<td><div id ="blanket" style="display:none;">
						</div><div id = "pst' . $i . '">
						<div id ="popUpDiv' . $i . '" style="display:none; 	color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
						<a href="#" onclick="popup(\'popUpDiv' . $i . '\')"><img src="' . $gp[$x] . '.jpg"><br /><span class = "clospic">Click on the picture to close it</span></a>
						</div>
						<a href="#"     onmouseover="rollover(\'fs' . $i . '\', \'pix/nav/fsa.gif\'); return false" 
    									onmouseout="rollover(\'fs' . $i . '\', \'pix/nav/fs.gif\'); return false"
    									onclick="popup(\'popUpDiv' . $i . '\')"><img name = "fs' . $i . '" src = "pix/nav/fs.gif"></a></div></td>
						';
					}
				
				
				
				}	
				$pst .= '</tr>
		</table>
		</div>

	
	';
	}
	elseif ($pstype == 'gen') {
		/*
		$pst = '<div class = "fs">
	<table border = "1" id = "pst">
		
		<tr>
			<td id = "pst1"><a href="profile.php?id=' . PST_1_PS . '"     onmouseover="rollover(\'fs1\', \'pix/nav/fsa.gif\'); return false" 
    																				onmouseout="rollover(\'fs1\', \'pix/nav/fs.gif\'); return false">
    																				<img name = "fs1" src = "pix/nav/fs.gif" width = "83" height = "99"></a></td>		
			<td id = "pst2"><a href="profile.php?id=' . PST_2_PS . '"     onmouseover="rollover(\'fs2\', \'pix/nav/fsa.gif\'); return false" 
    																				onmouseout="rollover(\'fs2\', \'pix/nav/fs.gif\'); return false">
    																				<img name = "fs2" src = "pix/nav/fs.gif" width = "83" height = "99"></a></td>		
			<td id = "pst3"><a href="profile.php?id=' . PST_3_PS . '"     onmouseover="rollover(\'fs3\', \'pix/nav/fsa.gif\'); return false" 
    																				onmouseout="rollover(\'fs3\', \'pix/nav/fs.gif\'); return false">
    																				<img name = "fs3" src = "pix/nav/fs.gif" width = "83" height = "99"></a></td>		
			<td id = "pst4"><a href="profile.php?id=' . PST_4_PS . '"     onmouseover="rollover(\'fs4\', \'pix/nav/fsa.gif\'); return false" 
    																				onmouseout="rollover(\'fs4\', \'pix/nav/fs.gif\'); return false">
    																				<img name = "fs4" src = "pix/nav/fs.gif" width = "83" height = "99"></a></td>	
			<td id = "pst5"><a href="profile.php?id=' . PST_5_PS . '"     onmouseover="rollover(\'fs5\', \'pix/nav/fsa.gif\'); return false" 
    																				onmouseout="rollover(\'fs5\', \'pix/nav/fs.gif\'); return false">
    																				<img name = "fs5" src = "pix/nav/fs.gif" width = "83" height = "99"></a></td>
			<td id = "pst6"><a href="profile.php?id=' . PST_6_PS . '"     onmouseover="rollover(\'fs6\', \'pix/nav/fsa.gif\'); return false" 
    																				onmouseout="rollover(\'fs6\', \'pix/nav/fs.gif\'); return false">
    																				<img name = "fs6" src = "pix/nav/fs.gif" width = "83" height = "99"></a></td>
			<td id = "pst7"><a href="profile.php?id=' . PST_7_PS . '"     onmouseover="rollover(\'fs7\', \'pix/nav/fsa.gif\'); return false" 
    																				onmouseout="rollover(\'fs7\', \'pix/nav/fs.gif\'); return false">
    																				<img name = "fs7" src = "pix/nav/fs.gif" width = "83" height = "99"></a></td>
			<td id = "pst8"><a href="profile.php?id=' . PST_8_PS . '"     onmouseover="rollover(\'fs8\', \'pix/nav/fsa.gif\'); return false" 
    																				onmouseout="rollover(\'fs8\', \'pix/nav/fs.gif\'); return false">
    																				<img name = "fs8" src = "pix/nav/fs.gif" width = "83" height = "99"></a></td>
			<td id = "pst9"><a href="profile.php?id=' . PST_9_PS . '"     onmouseover="rollover(\'fs9\', \'pix/nav/fsa.gif\'); return false" 
    																				onmouseout="rollover(\'fs9\', \'pix/nav/fs.gif\'); return false">
    																				<img name = "fs9" src = "pix/nav/fs.gif" width = "83" height = "99"></a></td>
			<td id = "pst10"><a href="profile.php?id=' . PST_10_PS . '"     onmouseover="rollover(\'fs10\', \'pix/nav/fsa.gif\'); return false" 
    																				onmouseout="rollover(\'fs10\', \'pix/nav/fs.gif\'); return false">
    																				<img name = "fs10" src = "pix/nav/fs.gif" width = "83" height = "99"></a></td>
		</tr>
		
	</table>
</div>';
	*/
	$pst = '<div class = "fs">
	<table border = "1" id = "pst">
		
		<tr>
			<td id = "pst1"><a href="#"     onmouseover="rollover(\'fs1\', \'pix/nav/fsa.gif\'); return false" 
    																				onmouseout="rollover(\'fs1\', \'pix/nav/fs.gif\'); return false">
    																				<img name = "fs1" src = "pix/nav/fs.gif" width = "83" height = "99"></a></td>		
			<td id = "pst2"><a href="#"     onmouseover="rollover(\'fs2\', \'pix/nav/fsa.gif\'); return false" 
    																				onmouseout="rollover(\'fs2\', \'pix/nav/fs.gif\'); return false">
    																				<img name = "fs2" src = "pix/nav/fs.gif" width = "83" height = "99"></a></td>		
			<td id = "pst3"><a href="#"     onmouseover="rollover(\'fs3\', \'pix/nav/fsa.gif\'); return false" 
    																				onmouseout="rollover(\'fs3\', \'pix/nav/fs.gif\'); return false">
    																				<img name = "fs3" src = "pix/nav/fs.gif" width = "83" height = "99"></a></td>		
			<td id = "pst4"><a href="#"     onmouseover="rollover(\'fs4\', \'pix/nav/fsa.gif\'); return false" 
    																				onmouseout="rollover(\'fs4\', \'pix/nav/fs.gif\'); return false">
    																				<img name = "fs4" src = "pix/nav/fs.gif" width = "83" height = "99"></a></td>	
			<td id = "pst5"><a href="#"     onmouseover="rollover(\'fs5\', \'pix/nav/fsa.gif\'); return false" 
    																				onmouseout="rollover(\'fs5\', \'pix/nav/fs.gif\'); return false">
    																				<img name = "fs5" src = "pix/nav/fs.gif" width = "83" height = "99"></a></td>
			<td id = "pst6"><a href="#"     onmouseover="rollover(\'fs6\', \'pix/nav/fsa.gif\'); return false" 
    																				onmouseout="rollover(\'fs6\', \'pix/nav/fs.gif\'); return false">
    																				<img name = "fs6" src = "pix/nav/fs.gif" width = "83" height = "99"></a></td>
			<td id = "pst7"><a href="#"     onmouseover="rollover(\'fs7\', \'pix/nav/fsa.gif\'); return false" 
    																				onmouseout="rollover(\'fs7\', \'pix/nav/fs.gif\'); return false">
    																				<img name = "fs7" src = "pix/nav/fs.gif" width = "83" height = "99"></a></td>
			<td id = "pst8"><a href="#"     onmouseover="rollover(\'fs8\', \'pix/nav/fsa.gif\'); return false" 
    																				onmouseout="rollover(\'fs8\', \'pix/nav/fs.gif\'); return false">
    																				<img name = "fs8" src = "pix/nav/fs.gif" width = "83" height = "99"></a></td>
			<td id = "pst9"><a href="#"     onmouseover="rollover(\'fs9\', \'pix/nav/fsa.gif\'); return false" 
    																				onmouseout="rollover(\'fs9\', \'pix/nav/fs.gif\'); return false">
    																				<img name = "fs9" src = "pix/nav/fs.gif" width = "83" height = "99"></a></td>
			<td id = "pst10"><a href="#"     onmouseover="rollover(\'fs10\', \'pix/nav/fsa.gif\'); return false" 
    																				onmouseout="rollover(\'fs10\', \'pix/nav/fs.gif\'); return false">
    																				<img name = "fs10" src = "pix/nav/fs.gif" width = "83" height = "99"></a></td>
		</tr>
		
	</table>
</div>';
	}
	elseif ($pstype == 'htnrooms'){
		$pst = '<div class = "fs">

		<table border = "1" id = "pst">
			<tr>
					<td id = "pst1"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv1" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv1\')"><img src="pix/rooms/IMG_2398.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#"     onmouseover="rollover(\'fs1\', \'pix/nav/fsa.gif\'); return false" 
    								onmouseout="rollover(\'fs1\', \'pix/nav/fs.gif\'); return false"
    								onclick="popup(\'popUpDiv1\')"><img name = "fs1" src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst2"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv2" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv2\')"><img src="pix/rooms/IMG_2406.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#"     onmouseover="rollover(\'fs2\', \'pix/nav/fsa.gif\'); return false" 
    								onmouseout="rollover(\'fs2\', \'pix/nav/fs.gif\'); return false"
    								onclick="popup(\'popUpDiv2\')"><img name = "fs2" src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst3"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv3" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv3\')"><img src="pix/rooms/IMG_2434.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#"     onmouseover="rollover(\'fs3\', \'pix/nav/fsa.gif\'); return false" 
    								onmouseout="rollover(\'fs3\', \'pix/nav/fs.gif\'); return false"
    								onclick="popup(\'popUpDiv3\')"><img name = "fs3" src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst4"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv4" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv4\')"><img src="pix/rooms/IMG_2435.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#"     onmouseover="rollover(\'fs4\', \'pix/nav/fsa.gif\'); return false" 
    								onmouseout="rollover(\'fs4\', \'pix/nav/fs.gif\'); return false"
    								onclick="popup(\'popUpDiv4\')"><img name = "fs4" src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst5"><div id ="blanket" style="display:none;style="display:none;">
					</div>
					<div id ="popUpDiv5" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv5\')"><img src="pix/rooms/IMG_2436.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#"     onmouseover="rollover(\'fs5\', \'pix/nav/fsa.gif\'); return false" 
    								onmouseout="rollover(\'fs5\', \'pix/nav/fs.gif\'); return false"
    								onclick="popup(\'popUpDiv5\')"><img name = "fs5" src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst6"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv6" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv6\')"><img src="pix/rooms/IMG_2437.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#"     onmouseover="rollover(\'fs6\', \'pix/nav/fsa.gif\'); return false" 
    								onmouseout="rollover(\'fs6\', \'pix/nav/fs.gif\'); return false"
    								onclick="popup(\'popUpDiv6\')"><img name = "fs6" src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst7"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv7" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv7\')"><img src="pix/rooms/IMG_2439.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#"     onmouseover="rollover(\'fs7\', \'pix/nav/fsa.gif\'); return false" 
    								onmouseout="rollover(\'fs7\', \'pix/nav/fs.gif\'); return false"
    								onclick="popup(\'popUpDiv7\')"><img name = "fs7" src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst8"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv8" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv8\')"><img src="pix/rooms/IMG_2441.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#"     onmouseover="rollover(\'fs8\', \'pix/nav/fsa.gif\'); return false" 
    								onmouseout="rollover(\'fs8\', \'pix/nav/fs.gif\'); return false"
    								onclick="popup(\'popUpDiv8\')"><img name = "fs8" src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst9"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv9" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv9\')"><img src="pix/rooms/IMG_2445.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#"     onmouseover="rollover(\'fs9\', \'pix/nav/fsa.gif\'); return false" 
    								onmouseout="rollover(\'fs9\', \'pix/nav/fs.gif\'); return false"
    								onclick="popup(\'popUpDiv9\')"><img name = "fs9" src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst10"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv10" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv10\')"><img src="pix/rooms/IMG_2446.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#"     onmouseover="rollover(\'fs10\', \'pix/nav/fsa.gif\'); return false" 
    								onmouseout="rollover(\'fs10\', \'pix/nav/fs.gif\'); return false"
    								onclick="popup(\'popUpDiv10\')"><img name = "fs10" src = "pix/nav/fs.gif"></a></td>
					</tr>
		</table>
	</div>
';

	}
	
	elseif ($pstype == 'ren'){
		$pst = '<div class = "fs">

		<table border = "1" id = "pst">
			<tr>
				<td id = "pst1"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv1" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv1\')"><img src="pix/renov/IMG_2223.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#"    onmouseover="rollover(\'fs1\', \'pix/nav/fsa.gif\'); return false" 
    								onmouseout="rollover(\'fs1\', \'pix/nav/fs.gif\'); return false"
    								 onclick="popup(\'popUpDiv1\')"><img name = "fs1" src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst2"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv2" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv2\')"><img src="pix/renov/IMG_2227.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#"    onmouseover="rollover(\'fs2\', \'pix/nav/fsa.gif\'); return false" 
    								onmouseout="rollover(\'fs2\', \'pix/nav/fs.gif\'); return false"
    								 onclick="popup(\'popUpDiv2\')"><img name = "fs2" src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst3"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv3" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv3\')"><img src="pix/renov/IMG_2229.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#"    onmouseover="rollover(\'fs3\', \'pix/nav/fsa.gif\'); return false" 
    								onmouseout="rollover(\'fs3\', \'pix/nav/fs.gif\'); return false"
    								 onclick="popup(\'popUpDiv3\')"><img name = "fs3" src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst4"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv4" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv4\')"><img src="pix/renov/IMG_2238.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#"    onmouseover="rollover(\'fs4\', \'pix/nav/fsa.gif\'); return false" 
    								onmouseout="rollover(\'fs4\', \'pix/nav/fs.gif\'); return false"
    								 onclick="popup(\'popUpDiv4\')"><img name = "fs4" src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst5"><div id ="blanket" style="display:none;style="display:none;">
					</div>
					<div id ="popUpDiv5" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv5\')"><img src="pix/renov/IMG_2247.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#"    onmouseover="rollover(\'fs5\', \'pix/nav/fsa.gif\'); return false" 
    								onmouseout="rollover(\'fs5\', \'pix/nav/fs.gif\'); return false"
    								 onclick="popup(\'popUpDiv5\')"><img name = "fs5" src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst6"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv6" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv6\')"><img src="pix/renov/IMG_2252.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>&nbsp;&nbsp;&nbsp;There goes Room 6!
					</div>
					<a href="#"    onmouseover="rollover(\'fs6\', \'pix/nav/fsa.gif\'); return false" 
    								onmouseout="rollover(\'fs6\', \'pix/nav/fs.gif\'); return false"
    								 onclick="popup(\'popUpDiv6\')"><img name = "fs6" src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst7"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv7" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv7\')"><img src="pix/renov/IMG_2278.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#"    onmouseover="rollover(\'fs7\', \'pix/nav/fsa.gif\'); return false" 
    								onmouseout="rollover(\'fs7\', \'pix/nav/fs.gif\'); return false"
    								 onclick="popup(\'popUpDiv7\')"><img name = "fs7" src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst8"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv8" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv8\')"><img src="pix/renov/IMG_2288.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#"    onmouseover="rollover(\'fs8\', \'pix/nav/fsa.gif\'); return false" 
    								onmouseout="rollover(\'fs8\', \'pix/nav/fs.gif\'); return false"
    								 onclick="popup(\'popUpDiv8\')"><img name = "fs8" src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst9"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv9" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv9\')"><img src="pix/renov/IMG_2294.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#"    onmouseover="rollover(\'fs9\', \'pix/nav/fsa.gif\'); return false" 
    								onmouseout="rollover(\'fs9\', \'pix/nav/fs.gif\'); return false"
    								 onclick="popup(\'popUpDiv9\')"><img name = "fs9" src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst10"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv10" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv10\')"><img src="pix/renov/IMG_2314.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>&nbsp;&nbsp;&nbsp;Now what do we do?
					</div>
					<a href="#"    onmouseover="rollover(\'fs10\', \'pix/nav/fsa.gif\'); return false" 
    								onmouseout="rollover(\'fs10\', \'pix/nav/fs.gif\'); return false"
    								 onclick="popup(\'popUpDiv10\')"><img name = "fs10" src = "pix/nav/fs.gif"></a></td>
			</tr>
		</table>
	</div>
	';
	}
	
	echo $pst;
	echo '
<!-- end photostrip -->
	';
}	

function get_tips_by_name($file_name){

	$query = 'select * FROM tooltips WHERE tip_file_url = "' . $file_name . '" ORDER BY tip_number';			
	$result = wcallq($query);
	
	if (!$result) {
		die ('Could not query the tooltips database : <br /> ' . mysql_error());
	}
	
	while($result_row = mysql_fetch_array($result, MYSQL_ASSOC)) {	
		$tips[$result_row['tip_name']]['tip_file_url']		=	$result_row['tip_file_url'];
		$tips[$result_row['tip_name']]['tip_number']		=	$result_row['tip_number'];
		$tips[$result_row['tip_name']]['tip_name']			=	$result_row['tip_name'];	
		$tips[$result_row['tip_name']]['tip_anchor']		= 	$result_row['tip_anchor'];	
		$tips[$result_row['tip_name']]['tip_text']			= 	$result_row['tip_text'];	
		$tips[$result_row['tip_name']]['tip_state']			= 	$result_row['tip_state'];	
	}

	return $tips;
}

function get_tips_by_number($file_name){

	$query = 'select * FROM tooltips WHERE tip_file_url = "' . $file_name . '" ORDER BY tip_number';			
	$result = wcallq($query);
	
	if (!$result) {
		die ('Could not query the tooltips database : <br /> ' . mysql_error());
	}
	
	while($result_row = mysql_fetch_array($result, MYSQL_ASSOC)) {	
		$tips[$result_row['tip_number']]['tip_file_url']		=	$result_row['tip_file_url'];
		$tips[$result_row['tip_number']]['tip_number']		=	$result_row['tip_number'];
		$tips[$result_row['tip_number']]['tip_name']			=	$result_row['tip_name'];	
		$tips[$result_row['tip_number']]['tip_anchor']		= 	$result_row['tip_anchor'];	
		$tips[$result_row['tip_number']]['tip_text']			= 	$result_row['tip_text'];	
		$tips[$result_row['tip_number']]['tip_state']			= 	$result_row['tip_state'];	
	}

	return $tips;
}

class Tooltip 	{
	
	var $file_name;
	var $text;
	var $anchor;

	function Tooltip($text){
	
		$this->text = $text;	
	}

	function write_tip($anchor){	
		$this->anchor = $anchor;
		echo '<a href="#" class="tt">' . $this->anchor . '<span class="tooltip"><span class="top"></span><span class="middle">' . $this->text . '</span><span class="bottom"></span></span></a>';
	}

}

function makeTip($tipname, $tips){
	if	($tips[$tipname]['tip_state']) {
	
		$tt = new Tooltip($tips[$tipname]['tip_text']);
		$tt ->write_tip($tips[$tipname]['tip_anchor']);
	}
	else {
		
		echo $tips[$tipname]['tip_anchor'];
	}
}

function menubox(){
	
	//	writes the navigation box at the top of the operator'e page
			
	echo 	'<table class="menubox">';
	echo	'<tr>';
	
			//	operator logged in
	echo	'<td class="menubox_cell">' . $_SESSION['first_name'] . ' is logged in</td>';
						
			//	drivers
	echo 	'<td class="menubox_cell"></td>';
	
	

	
			//	link to public web site
	echo 	'<td class="menubox_cell"><a href="index.php">Public web site</a></td>';
	
			//	Link to log out
	echo 	'<td class="menubox_cell"><a href="log_in.php?id=logout">Log out</a></td>';
	
	echo 	'</tr><tr>';
	
	echo 	'<td class="menubox_cell"></td>';
	
			//	edit shift
	echo 	'<td class="menubox_cell"><a href="' . ED_SHIFT . '">Edit shift</a></td>';
	

	echo 	'<td class="menubox_cell"></td>';


	echo 	'<td class="menubox_cell"></td>';
	
	echo 	'</tr>';
	echo 	'</table>';
	
}



function get_admin(){

	//	Gets all the details from the admin database
	
	$query = 'select * FROM admin WHERE adminref = "1"';
	
	//	This call is wcallq - this user cannot update the database
	$result = wcallq($query);
	
	while($result_row = mysql_fetch_array($result, MYSQL_ASSOC)) {	
		
		//	puts the required information into the array $admin
		
		$admin['adminref']				=	$result_row['adminref'];
		$admin['day_shift_start']		=	$result_row['day_shift_start'];
		$admin['loc']					=	$result_row['loc'];
		$admin['price_20']				=  	$result_row['price_20'];
		$admin['price_30']				=	$result_row['price_30'];
		$admin['price_45']				= 	$result_row['price_45'];
		$admin['price_60']				= 	$result_row['price_60'];
		$admin['price_bid']				= 	$result_row['price_bid'];
		$admin['u_20_nt']				= 	$result_row['u_20_nt'];
		$admin['u_20_dy']				= 	$result_row['u_20_dy'];
		$admin['t_20_nt']				= 	$result_row['t_20_nt'];
		$admin['t_20_dy']				= 	$result_row['t_20_dy'];
		$admin['u_30_nt']				=	$result_row['u_30_nt'];
		$admin['u_30_dy']				=	$result_row['u_30_dy'];
		$admin['t_30_nt']				=	$result_row['t_30_nt'];
		$admin['t_30_dy']				=	$result_row['t_30_dy'];
		$admin['u_45_nt']				=	$result_row['u_45_nt'];
		$admin['u_45_dy']				=	$result_row['u_45_dy'];
		$admin['t_45_nt']				=	$result_row['t_45_nt'];
		$admin['t_45_dy']				=	$result_row['t_45_dy'];
		$admin['u_60_nt']				=	$result_row['u_60_nt'];
		$admin['u_60_dy']				=	$result_row['u_60_dy'];
		$admin['t_60_nt']				=	$result_row['t_60_nt'];
		$admin['t_60_dy']				=	$result_row['t_60_dy'];
		$admin['u_bid_nt']				=	$result_row['u_bid_nt'];
		$admin['u_bid_dy']				=	$result_row['u_bid_dy'];
		$admin['t_bid_nt']				=	$result_row['t_bid_nt'];
		$admin['t_bid_dy']				=	$result_row['t_bid_dy'];
		$admin['cdm']					=	$result_row['cdm'];
		$admin['lbe']					=	$result_row['lbe'];
		$admin['spe']					=	$result_row['spe'];
		$admin['shft_fee']				=	$result_row['shft_fee'];
		$admin['tip_hdlfee_c']			=	$result_row['tip_hdlfee_c'];
		$admin['thf_rounded_up_to']		=	$result_row['thf_rounded_up_to'];
		$admin['ext_extra']				=	$result_row['ext_extra'];
		$admin['gst_rate']				=	$result_row['gst_rate'];
		$admin['disc_60_1']				=	$result_row['disc_60_1'];
		$admin['night_shift_start']		=	$result_row['night_shift_start'];
		$admin['grave_shift_start']		=	$result_row['grave_shift_start'];
		
	}		
	return $admin;
}

function buildViewAdmin($admin, $loc) {
	
	//	displays a table containing the admin info
	
	$table = '';
	


	$table .= '<table border = "0" class = "menubox_cell">
	';
	$table .= '<tr>
		<td class = "menubox_cell">Admin ref
		</td>
		<td class = "menubox_cell">' . $admin['adminref'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">Location		</td>
		<td class = "menubox_cell">' . $admin['loc'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">Price 20
		</td>
		<td class = "menubox_cell">' . $admin['price_20'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">Price 30
		</td>
		<td class = "menubox_cell">' . $admin['price_30'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">Price 45
		</td>
		<td class = "menubox_cell">' . $admin['price_45'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">Price 60
		</td>
		<td class = "menubox_cell">' . $admin['price_60'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">Price Bi double
		</td>
		<td class = "menubox_cell">' . $admin['price_bid'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">House 20 night
		</td>
		<td class = "menubox_cell">' . $admin['u_20_nt'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">House 20 day
		</td>
		<td class = "menubox_cell">' . $admin['u_20_dy'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">Girl 20 night
		</td>
		<td class = "menubox_cell">' . $admin['t_20_nt'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">Girl 20 day
		</td>
		<td class = "menubox_cell">' . $admin['t_20_dy'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">House 30 night
		</td>
		<td class = "menubox_cell">' . $admin['u_30_nt'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">House 30 day
		</td>
		<td class = "menubox_cell">' . $admin['u_30_dy'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">Girl 30 night
		</td>
		<td class = "menubox_cell">' . $admin['t_30_nt'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">Girl 30 day
		</td>
		<td class = "menubox_cell">' . $admin['t_30_dy'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">House 45 night
		</td>
		<td class = "menubox_cell">' . $admin['u_45_nt'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">House 45 day
		</td>
		<td class = "menubox_cell">' . $admin['u_45_dy'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">Girl 45 night
		</td>
		<td class = "menubox_cell">' . $admin['t_45_nt'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">Girl 45 day
		</td>
		<td class = "menubox_cell">' . $admin['t_45_dy'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">House 60 night
		</td>
		<td class = "menubox_cell">' . $admin['u_60_nt'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">House 60 day
		</td>
		<td class = "menubox_cell">' . $admin['u_60_dy'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">Girl 60 night
		</td>
		<td class = "menubox_cell">' . $admin['t_60_nt'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">Girl 60 day
		</td>
		<td class = "menubox_cell">' . $admin['t_60_dy'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">House bi double night
		</td>
		<td class = "menubox_cell">' . $admin['u_bid_nt'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">House bi double day
		</td>
		<td class = "menubox_cell">' . $admin['u_bid_dy'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">Girl bi double night
		</td>
		<td class = "menubox_cell">' . $admin['t_bid_nt'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">Girl bi double day
		</td>
		<td class = "menubox_cell">' . $admin['t_bid_dy'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">Pack 12 condoms
		</td>
		<td class = "menubox_cell">' . $admin['cdm'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">Tube lube
		</td>
		<td class = "menubox_cell">' . $admin['lbe'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">Sponge
		</td>
		<td class = "menubox_cell">' . $admin['spe'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">Shift fee
		</td>
		<td class = "menubox_cell">' . $admin['shft_fee'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">Tip handling fee - credit card
		</td>
		<td class = "menubox_cell">' . $admin['tip_hdlfee_c'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">THF CC Rounded up to
		</td>
		<td class = "menubox_cell">' . $admin['thf_rounded_up_to'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">Extension extra pay
		</td>
		<td class = "menubox_cell">' . $admin['ext_extra'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "menubox_cell">GST rate
		</td>
		<td class = "menubox_cell">' . number_format($admin['gst_rate'], 2) . '
		</td>
	</tr>
	';

	$table .= '<tr>
		<td class = "menubox_cell">Discount 60 Rate 1
		</td>
		<td class = "menubox_cell">' . $admin['disc_60_1']. '
		</td>
	</tr>
	';

	$table .= '
</table>

';

	echo $table;
}								//	format corrected
	
?>










