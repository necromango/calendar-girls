<?php
require_once('includes/db_worx.php');
require_once('includes/cgdefs.php');

function picshop_index($gp) {
			// this function displays a table of the available images of $gp['girl_id']
			// if there are no pictures to display the function will never be called
			//	because we test for $gp['has_pix'] on the page picsop.php
			
			
			
	
	echo '
	<!-- start function picshop_index($gp) -->
	';
	echo '<table class = "main_list" border = "1">
		<tr>
			
			<td colspan = "5">
				<p class = "medtext">';
				//	makeTip('photostrip', $tips);
				echo '</p>
			</td>
			<td>
				<p class = "medtext">';
				//	makeTip('incoming', $tips);
				echo '</p>
			</td>
		</tr>
		<tr>
			';
		
	//	Loop through the images - there will be no more than MAXIM
	//	MAXIM is defined in cgdefs.php
	$movable = 0;
	for($p = 1; $p < MAXIM + 1; $p++){
		$x = 'fs' . $p;
		if ($gp[$x] == '') {
			$opt[$p] = 1;
			$movable = 1;
		}
		else $opt[$p] = 0;
	
	}	
		
		
	for($j = 1; $j < MAXIM + 1; $j++){
		$x = 'fs' . $j;
		if ($gp[$x] == '') {

				echo '
			<td valign = "top">	
				<div class = "text_center">
				<div class = "medtext">' . $j . '</div><div class = "picshop_ed_strip">vacant<br />frame</div>
				</div>
			</td>';
			
				if($j == 5){
					j_is_five($gp);
				}
			}	
			else {
				$picname = $gp[$x] . PIC_SFX; 	//	$picname is the rel url of the image file
				 //	echo '<p>' . $gp[$x] . PIC_SFX . '</p>';
				 $khgf = 0;
				if(file_exists($picname)){ 
					$khgf = 1;
					$size = getimagesize($picname);	//	$size is an array of details of the image
					$fsize = round(filesize($picname) / 1000);	//	gets the file size
				}
				
				// get the thumbnail details to write $tn - the img tag
				if(file_exists($gp[$x] . 'tn' . PIC_SFX)){
					$tn_size = getimagesize($gp[$x] . 'tn' . PIC_SFX);
					//	shring the thumbnails for the picshop grid
					//	this alters the img size tags - not the file itself
					if($tn_size[0] > 100){
						$ratio = 100 / $tn_size[0];
						$tnw = round($tn_size[0] * $ratio);
						$tnh = round($tn_size[1] * $ratio);
					}
					else {
						$tnw = $tn_size[0];
						$tnh = $tn_size[1];
					}
					$tn = '<img name = "' . $gp['girl_id'] . '" id = "picshop_cell_' . $j . '" width="' . $tnw . '" height="' . $tnh . '" src="' . PIXPROF . $gp['girl_id'] . '/' . $gp['girl_id'] .  $j . 'tn' . PIC_SFX . '">';
				}
				else {
					$tn = 'No thumbnail';
					
				}
				//tn0 is : ' . $tn_size[0] . '<br /> tn1 is : ' . $tn_size[1] . ' ratio is : ' . $ratio . ' 
				//	so now we write the row
				echo '
			<td valign = "top">	
				<div><div class = "medtext">' . $j . '
				</div><div id ="blanket" style="display:none;">
					</div>
				<div class = "picshop_ed_strip"><a href="' . $_SERVER['PHP_SELF'] . '?id=del_pic:' . $gp['girl_id'] . '|' . $j . '"><img width="16" height="16" src="pix/nav/drop.png"></a>  <a href="' . $_SERVER['PHP_SELF'] . '?id=set_main:' . $gp['girl_id'] . '|' . $j . '">set as main</a>';
				if ($movable){
				echo '<form action="' . $_SERVER['PHP_SELF'] . '" method = "get">
					<select name="move_to">';
				
					for($a = 1; $a < MAXIM + 1; $a++){

    					if ($opt[$a] == 1) {
    			
    							echo '<option value="' . $a . '">' . $a . '</option>
    						';
    					
    					}
    				}
					echo '</select> 
					<input type = "hidden" name = "move_pic" value = "move_pic" />
					<input type = "hidden" name = "girl_id" value = "' . $gp['girl_id'] . '" />
					<input type = "hidden" name = "pic_from" value = "' . $j . '" />
					<input type="submit" name="submit" value="Move">
					</form>';
				}
				
				echo '</div>
					<br />
					<div id ="popUpDiv' . $j . '" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 150px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : solid grey 1px;">
					<a href="#" onclick="popup(\'popUpDiv' . $j . '\')">
					
					<img src="' . $picname . '"></a>
					<br />
					<div>
					<table border = "0">
						<tr>
							<td colspan = "2"><span class = "medtext">Image no. ' . $j . ' of ' . $gp['name'] . '</span>
							<hr class = "picshop_hr">
							</td>
						</tr>
						<tr>
							<td>
								File name : 
							</td>
							<td>
								' . $gp['girl_id'] . $j . '.jpg
							</td>
						</tr>
						<tr>
						<tr>
							<td>
								File size : 
							</td>
							<td>
								' . $fsize . ' kb
							</td>
						</tr>
						<tr>
							<td>
								Directory :
							</td>
							<td>
								' . PIXPROF . $gp['girl_id'] . '/
							</td>
						</tr>
						<tr>
							<td>
								Width : 
							</td>
							<td>
								' . $size[0] . ' pixels
							</td>
						</tr>
						<tr>
							<td>
								Height : 
							</td>
							<td>
								' . $size[1] . ' pixels
							</td>
						</tr>
					</table>
					</div>
					<span class = "clospic">Click on the picture to close it</span>
					
					
					</div>
				<div class = "tn_pic"><a href ="#" onclick="popup(\'popUpDiv' . $j . '\')">' . $tn . '</a></div></div>
				';										

			echo '</td>
			';
			if ($j == 5){
				j_is_five($gp);
			}
		}
	}
	echo '<td>';
	
	if(file_exists($_FILES['upload_file']['tmp_name'])){
					
					move_uploaded_file($_FILES['upload_file']['tmp_name'], PIXPROF . $gp['girl_id'] . '/temp/temp.jpg');
					
					
				// make sure $picname is looking in the right place 
				// ie - in the girl's temp dir.
				
				$picname = PIXPROF . $gp['girl_id'] . '/temp/temp.jpg'; 	//	$picname is the rel url of the image file
				$size = getimagesize($picname);	//	$size is an array of details of the image
				$fsize = round(filesize($picname) / 1000);	//	gets the file size
												//	$size[0] is the width and $size[1] is the height
				$big_dim = 0;
				if ($size[0] > $size[1]){		// if width > height
					$big_dim = $size[0];		// width is the bigger dimension
				} else {
					$big_dim = $size[1];		// otherwise height is the bigger dimension
				}
				
				//	PMAXDIM is the maximum allowable dimension (either width or height) of an 
				//	image file - it is defined in cgdefs.php
				//	The following two lines set the width ($tnw) and height ($tnh) of the thumbnails
				//	Max allowable dimension of the thumbnails (either width or height) -  
				//	it is defined in cgdefs.php as MAX_TN_DIM
				//	this uses the actual images for the thumbnails but keeps the correct aspect ratio
				
				$tnw = round(($size[0] / ($big_dim / PMAXDIM)) / 4);
				$tnh = round(($size[1] / ($big_dim / PMAXDIM)) / 4);
				
				//	so now we write the row
				echo '
	
				<div><div class = "text_center">' . $_FILES['file_upload']['name'] . '</div><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDivtemp" style="display:none; 	color : white; padding : 10px; text-align : center; position : absolute; #top : 150px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : solid grey 1px;">
					<a href="#" onclick="popup(\'popUpDivtemp\')">
					
					<img src="' . $picname . '"></a>
					<br />
					<div>
					<table border = "0">
						<tr>
							<td colspan = "2"><span class = "medtext">Temp image of ' . $gp['name'] . '</span>
							<hr class = "picshop_hr">
							</td>
						</tr>
						<tr>
							<td>
								File name : 
							</td>
							<td>
								temp.jpg
							</td>
						</tr>
						<tr>
						<tr>
							<td>
								File size : 
							</td>
							<td>
								' . $fsize . ' kb
							</td>
						</tr>
						
							<td>
								Directory :
							</td>
							<td>
								pix/temp/
							</td>
						</tr>
						<tr>
							<td>
								Width : 
							</td>
							<td>
								' . $size[0] . ' pixels
							</td>
						</tr>
							<td>
								Height : 
							</td>
							<td>
								' . $size[1] . ' pixels
							</td>
						</tr>
					</table>
					</div>
					<span class = "clospic">Click on the picture to close it</span>
					
					
					</div>
				<a href ="#" onclick="popup(\'popUpDivtemp\')">
				<img name = "temp" id = "picshop_cell_temp" src = ' . $picname . ' width = "' . $tnw . '" height = "' . $tnh . '"></a></div>
				<br />
				<a href = "' . $_SERVER['PHP_SELF'] . '?id=' . $gp['girl_id'] . '">Delete</a>
					
					
					<br />Choose a frame to put this picture into and then click  Go<br />If the thumbnaul shows the wrong picture at first - re-load the page.
					
					
				
	
				<br /><br />
				<form action="' . $_SERVER['PHP_SELF'] . '" method = "get">
				<select name="overw">';
				
				for($b = 1; $b < MAXIM + 1; $b++){
					if ($opt[$b] == 1) {
    					echo '<option value="' . $b . '">' . $b . '</option>
    						';
 					}
    			}

    			
    			
				echo '</select> 
				<input type = "hidden" name = "assign_pic" value = "assign_pic" />
				<input type = "hidden" name = "girl_id" value = "' . $gp['girl_id'] . '" />
				<input type="submit" name="submit" value="Go">
				</form>
				';	
					
	
				}
				else {
					if($movable){
					
					
						echo '<div id = "upload_box">
						<form action="picshop.php?id=' . $gp['girl_id'] . '" method="post" enctype="multipart/form-data">
						<br /><br />
						';
					// makeTip('upload', $tips);
					echo '<br /><br />
						<input type="file" name="upload_file">
						<br />
						<br />
						<input type="submit" name="submit" value="Submit">
						</form>
						</div>';
					}
					else {
						echo '<p>';
						//	makeTip('upload more', $tips);
						echo '</p>';
					}
				}
					
					
					
		echo '</td>
	</tr>
	</table>
';
		echo '
	<!-- end function picshop_index($gp) -->
	';
	
}

function j_is_five($gp) {
				if(file_exists(PIXPROF . $gp['girl_id'] . '/' . $gp['girl_id'] . MAIN_PIC . PIC_SFX)){
					echo '<td class = "text_center"><p>';
					//	makeTip('This is your main picture', $tips);
					echo '<div id = "main_pic_size_box"><a href="' . $_SERVER['PHP_SELF'] . '?id=del_main_pic:' . $gp['girl_id'] . '"><img width="16" height="16" src="pix/nav/drop.png"></a>';
					echo '';
					
					echo '<img width = "' . $gp['main_pic_width'] . '" height = "' . $gp['main_pic_height'] . '" src = "' . PIXPROF . $gp['girl_id'] . '/' . $gp['girl_id'] . MAIN_PIC . PIC_SFX . '"><br /><div class = "picshop_ed_strip">';
					
				
				
					echo '</div></td>
					</tr>
					<tr>
						';
				}
				else {
					echo '<td class = "text_center"><div id = "main_pic_size_box"><p>';
					//	makeTip('set main pic', $tips);
					echo '<br />Try to use one that fits the box as nearly as possible.</p></div></td>
					</tr>
					<tr>
						';
				}
			
}

function est_img_dir($gp){

	if(!file_exists(PIXPROF . $gp['girl_id'])){
									
		echo '<p>It does not exist...</p>';
										
		if(mkdir(PIXPROF . $gp['girl_id'], 0777)){
			
			echo '<p>... Made the directory' . PIXPROF . $gp['girl_id'] . '</p>';
											
			if(mkdir(PIXPROF . $gp['girl_id'] . '/temp', 0777)){
				echo '<p>... Made the directory ' . PIXPROF . $gp['girl_id'] . '/temp</p>';
											
			}
			else {
				echo '<p>Bollocks ... could not make ' . PIXPROF . $gp['girl_id'] . '/temp</p>';
			}
		}
		else {
			echo '<p>Bollocks ... could not make ' . PIXPROF . $gp['girl_id'] . '<br />$girl_id is : ' . $gp['girl_id'] . '</p>';
		}
	}
	else{
			//echo '<p>' . PIXPROF . $gp['girl_id'] .' already exists</p>';
			
	}

}

function assign_pic ($gp){
	
	 	//	echo '<p>Hey, we have arrived at the top of assign_pic()</p>';
	 	//	echo '<p>Inside assign_pic() $overw is : ' . $_GET['overw'] . '</p>';
	 	//	echo '<p>Inside assign_pic() $_GET[\'girl_id\'] is : ' . $_GET['girl_id'] . '</p>';
	
	$frame = $_GET['overw'];

	switch ($frame) {
		case 1: $fram = '1'; break;
		case 2: $fram = '2'; break;
		case 3: $fram = '3'; break;
		case 4: $fram = '4'; break;
		case 5: $fram = '5'; break;
		case 6: $fram = '6'; break;
		case 7: $fram = '7'; break;
		case 8: $fram = '8'; break;
		case 9: $fram = '9'; break;
		case 10: $fram = '10'; $frame = 10; break;

	}
	 //	echo '<p>$frame is now : ' . $frame . '</p>';
		
		$temp_pic = PIXPROF . $_GET['girl_id'] . '/temp/temp.jpg';
	if(file_exists($temp_pic)){
	
		 //	echo '<p>There is a temp file to move.</p>';
		
		$pic_file = PIXPROF . $_GET['girl_id'] . '/' . $_GET['girl_id'] . $frame;
		if(rename($temp_pic, $pic_file . PIC_SFX)){
			
			
			
			
			//$picname = PIXPROF . $gp['girl_id'] . '/' . $_GET['girl_id'] . $frame . '.jpg';
			
			$size = getimagesize($pic_file . PIC_SFX);	//	$size is an array of details of the image
			
			// Instantiate the class SimpleImage - ie. create an object
			$image = new SimpleImage();
			
			//	call the method load() from the class SimpleImage
			$image->load($pic_file . PIC_SFX);
			
			//	echo '$pic_file . PIC_SFX is : ' . $pic_file . PIC_SFX . '<br />$size[0] is : ' . $size[0] . '<br />$size[1] is : ' . $size[1] . '<br />';
			
			//	not sure about this bit
			//	I'm going to try here to re-size images that are too big to bring them down to PMAXDIM - which is 500 - either height or width
			
			if ($size[1] > MAXDIM || $size[0] > MAXDIM){
			
			
			
				//	echo '<p>Picture is too BIG !!<br /> I\'m going to resize it ...<br />Let\s have a look ...<br />';	
				if($size[1] > $size[0]){
					//	echo 'The larger dimension is ' . $size[1] . ' which is the height</br />Now I will attempt to bring it down to MAXDIM which is 500<br />';
					$image->resizeToHeight(PMAXDIM);
					$image->save($pic_file . PIC_SFX);
					
				}
				else{
					//	echo 'The larger dimension is ' . $size[0] . ' which is the width<br />Now I will attempt to bring it down to MAXDIM which is 500<br />';
					$image->resizeToWidth(PMAXDIM);
					$image->save($pic_file . PIC_SFX);
				}
				
				
				}
				
			// This is where we make the thumbnail;
			//	if the pic size is more than the max allowable - re-size the image
			($size[1] / $size[0]) > ( MAX_TN_HT / MAX_TN_WT) ? $image->resizeToWidth(MAX_TN_WT): $image->resizeToHeight(MAX_TN_HT);
			
			$image->save($pic_file . 'tn' . PIC_SFX);


			// if the has_image flag is unset in the database, set it
			
			if ($gp['has_pix'] == 0){
				$query = 'UPDATE girls SET has_pix = "1" WHERE girl_ID = ' . $_GET['girl_id'] . ';';
				$result = wwopq($query);
				if (!$result) {
					die ('Could not update the database : <br /> ' . mysql_error());
				}
			}
			
			//	set the img_details field in the database
			//	echo '<br />$fram is now : ' . $fram . '';
			$query = 'UPDATE girls SET fs' . $fram . ' = "' . PIXPROF . $gp['girl_id'] . '/' . $gp['girl_id'] . $frame . '" WHERE girl_ID = ' . $_GET['girl_id'] . ';';
			$result = wwopq($query);
			if (!$result) {
				die ('Could not update the database here: <br /> ' . mysql_error());
			}
			
			$query = 'UPDATE girls SET fs' . $fram . '_int = "3" WHERE girl_ID = ' . $_GET['girl_id'] . ';';
			$result = wwopq($query);
			if (!$result) {
				die ('Could not (set fs_int) update the database here: <br /> ' . mysql_error());
			}
			
		}
		//	this is where you end up when you can't assign the incoming picture - and?
	}
}

function del_pic($girl_id, $pic_2_del){
			
	switch ($pic_2_del) {
		case 1: $fram = '1'; break;
		case 2: $fram = '2'; break;
		case 3: $fram = '3'; break;
		case 4: $fram = '4'; break;
		case 5: $fram = '5'; break;
		case 6: $fram = '6'; break;
		case 7: $fram = '7'; break;
		case 8: $fram = '8'; break;
		case 9: $fram = '9'; break;
		case 10: $fram = '10'; $frame = 10; break;
	}
	$pic_2_del = PIXPROF . $girl_id . '/' . $girl_id . $pic_2_del;
	
	if(file_exists($pic_2_del . PIC_SFX)){
		unlink($pic_2_del . PIC_SFX);
	}
	if(file_exists($pic_2_del . 'tn' . PIC_SFX)){
		unlink($pic_2_del . 'tn' . PIC_SFX);
	}
	
	
	$query = 'UPDATE girls SET fs' . $fram . ' = "" WHERE girl_ID = ' . $girl_id . ';';
	$result = wwopq($query);
	if (!$result) {
		die ('Could not update the database : <br /> ' . mysql_error());
	}
	$query = 'UPDATE girls SET fs' . $fram . '_int = "" WHERE girl_ID = ' . $girl_id . ';';
	$result = wwopq($query);
	if (!$result) {
		die ('Could not (delete fs_int) update the database here: <br /> ' . mysql_error());
	}
}

function del_main_pic($girl_id){
			

	$pic_2_del = PIXPROF . $gp['girl_id'] . '/' . $gp['girl_id'] . '100' . PIC_SFX;
	
	if(file_exists($pic_2_del . PIC_SFX)){
		unlink($pic_2_del . PIC_SFX);
	}
}
	
function move_pic($gp, $from, $to){
		
	//	echo '<p>Hey, we have arrived at the top of move_pic()</p>';
	//	echo '<p>Inside move_pic() $from is : ' . $from . '</p>';
	//	echo '<p>Inside move_pic() $to is : ' . $to . '</p>';
	
	// This function works but sometimes you have to reload the page to get the image to update in the preview pane
	
	$gpfrom = frame_to_fram($from);
	$gpto	= frame_to_fram($to);
	if($to == 0){
		$to = 10;
	}
	
	
		//	echo '<p>Inside move_pic() $gpfrom is : ' . $gpfrom . '</p>';	
		//	echo '<p>Inside move_pic() $gpto is : ' . $gpto . '</p>';

	$picfrom 	= 	PIXPROF . $gp['girl_id'] . '/' . $gp['girl_id'] . $from . PIC_SFX;
	$picfromtn	=	PIXPROF . $gp['girl_id'] . '/' . $gp['girl_id'] . $from . 'tn' . PIC_SFX;
	$picto		=	PIXPROF . $gp['girl_id'] . '/' . $gp['girl_id'] . $to . PIC_SFX;
	$pictotn	=	PIXPROF . $gp['girl_id'] . '/' . $gp['girl_id'] . $to . 'tn' . PIC_SFX;
	
		//	echo '<p>Inside move_pic() $picfrom is : ' . $picfrom . '</p>';
		//	echo '<p>Inside move_pic() $picfromtn is : ' . $picfromtn . '</p>';
		//	echo '<p>Inside move_pic() $picto is : ' . $picto . '</p>';
		//	echo '<p>Inside move_pic() $pictotn is : ' . $pictotn . '</p>';
		
	if(file_exists($picfrom)){
		rename($picfrom, $picto);
	}
	if(file_exists($picfromtn)){
		rename($picfromtn, $pictotn);
	}
	//	set the img_details field in the database
			
	$query = 'UPDATE girls SET fs' . $gpfrom . ' = "" WHERE girl_ID = ' . $gp['girl_id'] . ';';
	$result = wwopq($query);
	if (!$result) {
		die ('Could not update the database - clearing from field : <br /> ' . mysql_error());
	}
	$query = 'UPDATE girls SET fs' . $gpto . ' = "' . PIXPROF . $gp['girl_id'] . '/' . $gp['girl_id'] . $to . '" WHERE girl_ID = ' . $gp['girl_id'] . ';';
	$result = wwopq($query);
	if (!$result) {
		die ('Could not update the database entering to field: <br /> ' . mysql_error());
	}
}

function frame_to_fram ($frame) {
		echo '$frame is : ' . $frame . '';
	switch ($frame) {
		case 1: $fram = '1'; break;
		case 2: $fram = '2'; break;
		case 3: $fram = '3'; break;
		case 4: $fram = '4'; break;
		case 5: $fram = '5'; break;
		case 6: $fram = '6'; break;
		case 7: $fram = '7'; break;
		case 8: $fram = '8'; break;
		case 9: $fram = '9'; break;
		case 10: $fram = '10'; $frame = 10; break;			
	}
	return $fram;
}

class SimpleImage {
   // these notes are my own - getting to grips with OOP
   //	my book says there has to be a constructor but there doesn't appear to be one here
   //	declare the variables - the default is public
   var $image;
   var $image_type;
 
   function load($filename) {
   		//	getimagesize() is a standard php function
   		//	$image_info is an array [0] is width, [1] is height, and [2] is type
      $image_info = getimagesize($filename);

      $this->image_type = $image_info[2];
      // if it's a jpeg
      if( $this->image_type == IMAGETYPE_JPEG ) {
      	//	assign the image file to $this->image
         $this->image = imagecreatefromjpeg($filename);
         
      } elseif( $this->image_type == IMAGETYPE_GIF ) {
         $this->image = imagecreatefromgif($filename);
         
      } elseif( $this->image_type == IMAGETYPE_PNG ) {
         $this->image = imagecreatefrompng($filename);
      }
   }
   
   function save($filename, $image_type=IMAGETYPE_JPEG, $compression=75, $permissions=null) {
   		// this will show and save the image
      if( $image_type == IMAGETYPE_JPEG ) {
      	//	imagejpeg — Output image to browser or file 
      	//	if $filename is specified the file will be saved to that location
         imagejpeg($this->image,$filename,$compression);
         
      } elseif( $image_type == IMAGETYPE_GIF ) {
         imagegif($this->image,$filename); 
         
      } elseif( $image_type == IMAGETYPE_PNG ) {
         imagepng($this->image,$filename);
         
      }   
      if( $permissions != null) {
         chmod($filename,$permissions);
      }
   }
   
   function output($image_type=IMAGETYPE_JPEG) {
   	//	this one just shows it
      if( $image_type == IMAGETYPE_JPEG ) {
         imagejpeg($this->image);
      } elseif( $image_type == IMAGETYPE_GIF ) {
         imagegif($this->image);         
      } elseif( $image_type == IMAGETYPE_PNG ) {
         imagepng($this->image);
      }   
   }
   
   function getWidth() {
      return imagesx($this->image);
   }
   
   function getHeight() {
      return imagesy($this->image);
   }
   
   function resizeToHeight($height) {
      $ratio = $height / $this->getHeight();
      $width = $this->getWidth() * $ratio;
      $this->resize($width,$height);
   }
   
   function resizeToWidth($width) {
      $ratio = $width / $this->getWidth();
      $height = $this->getheight() * $ratio;
      $this->resize($width,$height);
   }
   
   function scale($scale) {
      $width = $this->getWidth() * $scale/100;
      $height = $this->getheight() * $scale/100; 
      $this->resize($width,$height);
   }
   
   function resize($width,$height) {
      $new_image = imagecreatetruecolor($width, $height);
      imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
      $this->image = $new_image;   
   } 
   
}

function set_main($gp, $pic_2_set){

	
	$image = new SimpleImage();
	
	$pic_file = PIXPROF . $gp['girl_id'] . '/' . $gp['girl_id'] . $pic_2_set . PIC_SFX;
	
	if(file_exists($pic_file)){
		$new_pic = PIXPROF . $gp['girl_id'] . '/' . $gp['girl_id'] . '100' . PIC_SFX;
		if(copy($pic_file, $new_pic)){
			
			$image->load($new_pic);
			
			$width = $image->getWidth();
			//	echo '<p>Width is : ' . $width . '</p>';
			
			$height = $image->getHeight();
			//	echo '<p>Height is : ' . $height . '</p>';
			

			if($width > $height){
				$image->resizeToWidth(MAIN_PIC_WIDTH);
				$image->save($new_pic);
				
				$query = 'UPDATE girls SET main_pic_width = "' . $image->getWidth() . '" WHERE girl_ID = ' .  $gp['girl_id'] . ';';
				$result = wwopq($query);
				if (!$result) {
					die ('Could not update main_picthe database : <br /> ' . mysql_error());
				}
				$query = 'UPDATE girls SET main_pic_height = "' . $image->getHeight() . '" WHERE girl_ID = ' .  $gp['girl_id'] . ';';
				$result = wwopq($query);
				if (!$result) {
					die ('Could not update main_picthe database : <br /> ' . mysql_error());
				}
			}
			else {
				$image->resizeToHeight(MAIN_PIC_HEIGHT);
				$image->save($new_pic);
				$query = 'UPDATE girls SET main_pic_width = "' . $image->getWidth() . '" WHERE girl_ID = ' .  $gp['girl_id'] . ';';
				$result = wwopq($query);
				if (!$result) {
					die ('Could not update main_picthe database : <br /> ' . mysql_error());
				}
				$query = 'UPDATE girls SET main_pic_height = "' . $image->getHeight() . '" WHERE girl_ID = ' .  $gp['girl_id'] . ';';
				$result = wwopq($query);
				if (!$result) {
					die ('Could not update main_picthe database : <br /> ' . mysql_error());
				}
			}
			
		}
	}

	
	
	
}
?>










