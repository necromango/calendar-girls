<?php
require_once('includes/db_worx.php');
require_once('includes/cgdefs.php');

function get_pix(){

	$query = 'select * FROM pix';			
	$result = wcallq($query);
	
	while($result_row = mysql_fetch_array($result, MYSQL_ASSOC)) {	
		
		$pix[$result_row['name']]['name']		=	$result_row['name'];
		$pix[$result_row['name']]['rel_url']	=	$result_row['rel_url'];
		$pix[$result_row['name']]['width']		=  	$result_row['width'];
		$pix[$result_row['name']]['height']		=	$result_row['height'];
		$pix[$result_row['name']]['alt']		= 	$result_row['alt'];
		$pix[$result_row['name']]['caption']	= 	$result_row['caption'];
		$pix[$result_row['name']]['cat_1']		= 	$result_row['cat_1'];
		$pix[$result_row['name']]['cat_2']		= 	$result_row['cat_2'];
		$pix[$result_row['name']]['comment']	= 	$result_row['comment'];
		$pix[$result_row['name']]['date']		= 	$result_row['date'];

	}		
	return $pix;
}

function write_pic($pic) {
	echo '<img width = "' . $pic['width'] . '" height = "' . $pic['height'] . '" src = "' . $pic['rel_url'] . '" alt = "' . $pic['alt'] . '">';	
}

function buildEditForm ($new, $girl_id){
	
	$gp = get_gp($girl_id);
	echo '<!-- start function buildEditForm ($new, $girl_id) -->
		
	';
	if ($new == 2){
		//	if $new is set to '2' that means we are editing an existing record
		
		//	We will use $res later to decide whether or
		//	not to hide the existing values in the form so that we can access them
		//	when we process the form
		$content = '<h2>Edit details here for : ' . $gp['name'] . '</h2>
	
	';
		$res = 1;
	}
	

	//	start putting the content to be written into $form
	//	use the $fld[] array to put the existing values into the fields of the form
	//	since $res has a value we will hide the existing values in hidden fields at the 
	//	end of the form and then retrieve them when we process the form and compare them 
	//	to what was put into the form. This way we know what needs to be updated and can
	//	take action accordingly
	//	There is also a hidden field 'editform' We check for this back on edit_who.php to 
	//	see whether or not the form has been submitted.
	
	$fld[0] = $gp['girl_id'];
	$fld[1] = $gp['name'];
	$fld[2] = $gp['age'];
	$fld[3] = $gp['height'];
	$fld[4] = $gp['size'];
	$fld[5] = $gp['hair'];
	$fld[6] = $gp['eyes'];
	$fld[7] = $gp['bust'];
	$fld[8] = $gp['oc'];
	$fld[9] = $gp['comments'];
	$fld[10]= $gp['girl_id'];
	
	
	$form 	= 	'<form action = "';
	$form 	.=	$_SERVER['PHP_SELF'];
	$form 	.= 	'" method = "GET">
	
	<table border = "0">
		
		<tr>
			<td>
				<label>Name
			</td>
			<td>
				<input type = "text" name = "name" value = "' . $gp['name'] . '" /></label><a href="' . $_SERVER['PHP_SELF'] . '"><div class = "right_txt"><img align = "right" src = "pix/nav/close.png"></a><a href="' . $_SERVER['PHP_SELF'] . '">Cancel</a></div></td>
		</tr>
		<tr>
			<td>
				<label>Age</td><td><input type = "text" name = "age" value = "' . $gp['age'] . '" /></label>
			</td>
		</tr>	
		<tr>
			<td>
				<label>Height</td><td><input type = "text" name = "height" value = "' . $gp['height'] . '" /></label>
			</td>
		</tr>	
		<tr>
			<td>
				<label>Size</td><td><input type = "text" name = "size" value = "' . $gp['size'] . '" /></label>
			</td>
		</tr>	
		<tr>
			<td>
				<label>Hair</td><td><input type = "text" name = "hair" value = "' . $gp['hair'] . '" /></label>
			</td>
		</tr>	
		<tr>
			<td>
				<label>Eyes</td><td><input type = "text" name = "eyes" value = "' . $gp['eyes'] . '" /></label>
			</td>
		</tr>	
		<tr>
			<td>
				<label>Bust</td><td><input type = "text" name = "bust" value = "' . $gp['bust'] . '" /></label>
			</td>
		</tr>	
		<tr>
			<td>
				<label>Outcalls</td><td><input type = "text" name = "outcalls" value = "' . $gp['oc'] . '" /></label>
			</td>
		</tr>	
		<tr>
			<td>
				<label>Comments</td><td><textarea rows = "5" cols = "40" name = "comments">' . $gp['comments'] .'</textarea></label>
			</td>
		</tr>	
		<tr>
			<td>
				<input type = "hidden" name = "editform" value = "1" />' . ($res ? '<input type = "hidden" name = "fld1" value = "' . $fld[1] . '" /><input type = "hidden" name = "fld2" value = "' . $fld[2] . '" /><input type = "hidden" name = "fld3" value = "' . $fld[3] . '" /><input type = "hidden" name = "fld4" value = "' . $fld[4] . '" /><input type = "hidden" name = "fld5" value = "' . $fld[5] . '" /><input type = "hidden" name = "fld6" value = "' . $fld[6] . '" /><input type = "hidden" name = "fld7" value = "' . $fld[7] . '" /><input type = "hidden" name = "fld8" value = "' . $fld[8] . '" /><input type = "hidden" name = "fld9" value = "' . $fld[9] . '" /><input type = "hidden" name = "fld10" value = "' . $fld[10] . '" />' : '') . '</td><td align = "right"><input type = "submit" value = "Go" />
			</td>
		</tr>			
	
	</table>
	
	';



			
	//	write the content
	echo $content;
	//	write the form
	echo $form;
	
	echo '<p>Click <a href="' . $_SERVER['PHP_SELF'] . '?id=del_g' . $fld[0] . '">HERE TO DELETE</a> the entire record for ' . $fld[1] . '
	<br />
	DO NOT do this unless you are sure it is what you want to do
	<br />
	It can\'t be undone</p>
	
<!-- end function buildEditForm ($new, $girl_id) -->

';

}									//	format corrected

function buildViewGirl($gp, $location) {

	$table = '';
	
	if ($location == 'edit') {
		$location = 'edit_who';
		$table = '<table><tr><td class = "xt"><a href="' . $_SERVER['PHP_SELF'] . '?id=del_g' . $gp['girl_id'] . '"><img src = "pix/nav/close.png"></a></td><td class = "xt"><a href="' . $_SERVER['PHP_SELF'] . '?id=signon_h' . $gp['girl_id'] . '">sign on htn</a></td><td class = "xt"><a href="' . $_SERVER['PHP_SELF'] . '?id=signon_t' . $gp['girl_id'] . '">sign on tga</a></td><td class = "xt"><a href="' . $_SERVER['PHP_SELF'] . '?id=ed_g' . $gp['girl_id'] . '"><img src = "pix/nav/lock.jpg"></a></td></tr></table>';
	}

	$table .= '<table border = "0" class = "med1text">
	';
	$table .= '<tr>
		<td class = "xt">
		</td>
		<td class = "xtx">' . $gp['name'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "xt">Age
		</td>
		<td class = "xt">' . $gp['age'] . '</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "xt">Height
		</td>
		<td class = "xt">' . $gp['height'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "xt">Size
		</td>
		<td class = "xt">' . $gp['size'] . '
		</td>	
	</tr>
	';
	$table .= '<tr>
		<td class = "xt">Hair
		</td>
		<td class = "xt">' . $gp['hair'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "xt">Eyes
		</td>
		<td class = "xt">' . $gp['eyes'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "xt">Bust
		</td>
		<td class = "xt">' . $gp['bust'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "xt">Outcalls
		</td>
		<td class = "xt">' . $gp['oc'] . '
		</td>
	</tr>
	';
	$table .= '<tr>
		<td class = "xt">
		</td>
		<td class = "xt"><hr>' . $gp['comments'] . '
		</td>
	</tr>
	';
	$table .= '
</table>

';

	echo $table;
}									//	format corrected

function upd_girls_from_form() {

	//	set $chg to 0 - we'll test for it later
	$chg = 0;	
	//	init $result
	$result = 0;
	
	//	$_GET['fld1'] will only have a value if we retrieved the existing values from the record
	//	this means we are modifying a record rather than creating a new one
	//	since it does have a value we now move on to test which fields in the form were modified
	if($_GET['fld1']) {

		//	if the retrieved value hidden in the form does not match what came from the form itself
		//	we know it must have been updated so we build a query to do just that
		if($_GET['fld1'] != $_GET['name']) {

			//	assign the new field value to $name			
			$name 	= $_GET['name'];

			//	$_GET['fld10'] is the primary key 'girl_ID' from the database			
			$query = 'UPDATE girls SET name = "' . $name . '" WHERE girl_ID = ' . $_GET['fld10'] . ';';

			
			$result = wwopq($query);
			if (!$result) {
				die ('Could not query the database : <br /> ' . mysql_error());
			}
			
		//	give $chg a value since something has been updated
		$chg = 1;
		
		}
		
		//	same for all the other fields
		
		if($_GET['fld2'] != $_GET['age']) {
			$age 	= $_GET['age'];
				$query = 'UPDATE girls SET age = "' . $age . '" WHERE girl_ID = ' . $_GET['fld10'] . ';';
				$result = wwopq($query);
				if (!$result) {
					die ('Could not query the database : <br /> ' . mysql_error());
				}
			$chg = 1;
		}
		if ($_GET['fld3'] != $_GET['height']) {
			$height = $_GET['height'];
				$query = 'UPDATE girls SET height = "' . $height . '" WHERE girl_ID = ' . $_GET['fld10'] . ';';
				$result = wwopq($query);
				if (!$result) {
					die ('Could not query the database : <br /> ' . mysql_error());
				}
			$chg = 1;
		}
		if ($_GET['fld4'] != $_GET['size']) {
			$size 	= $_GET['size'];
				$query = 'UPDATE girls SET size = "' . $size . '" WHERE girl_ID = ' . $_GET['fld10'] . ';';
				$result = wwopq($query);
				if (!$result) {
					die ('Could not query the database : <br /> ' . mysql_error());
				}
			$chg = 1;
		}
		if ($_GET['fld5'] != $_GET['hair']) {
			$hair 	= $_GET['hair'];
				$query = 'UPDATE girls SET hair = "' . $hair . '" WHERE girl_ID = ' . $_GET['fld10'] . ';';
				$result = wwopq($query);
				if (!$result) {
					die ('Could not query the database : <br /> ' . mysql_error());
				}
			$chg = 1;
		}
		if ($_GET['fld6'] != $_GET['eyes']) {
			$eyes 	= $_GET['eyes'];
				$query = 'UPDATE girls SET eyes = "' . $eyes . '" WHERE girl_ID = ' . $_GET['fld10'] . ';';
				$result = wwopq($query);
				if (!$result) {
					die ('Could not query the database : <br /> ' . mysql_error());
				}
			$chg = 1;
		}
		if ($_GET['fld7'] != $_GET['bust']) {
			$bust 	= $_GET['bust'];
				$query = 'UPDATE girls SET bust = "' . $bust . '" WHERE girl_ID = ' . $_GET['fld10'] . ';';
				$result = wwopq($query);
				if (!$result) {
					die ('Could not query the database : <br /> ' . mysql_error());
				}
			$chg = 1;
		}
		if ($_GET['fld8'] != $_GET['outcalls']) {
			$outcalls = $_GET['outcalls'];
				$query = 'UPDATE girls SET oc = "' . $outcalls . '" WHERE girl_ID = ' . $_GET['fld10'] . ';';
				$result = wwopq($query);
				if (!$result) {
					die ('Could not query the database : <br /> ' . mysql_error());
				}
			$chg = 1;
		}
		if ($_GET['fld9'] != $_GET['comments']) {
			$comments = $_GET['comments'];
				$query = 'UPDATE girls SET comments = "' . $comments . '" WHERE girl_ID = ' . $_GET['fld10'] . ';';
				$result = wwopq($query);
				if (!$result) {
					die ('Could not query the database : <br /> ' . mysql_error());
				}
			$chg = 1;
		}
		
		//	If there have been any changes - act accordingly
		if($chg) {
			echo '<p>You have successfully updated the records.</p>';
			echo '<p><a href="' . $SERVER['PHP_SELF'] . '?id="><img src = "pix/nav/go.png"></a></p>';
			
		//	if nothing was changed ...	
		} else {
			echo '<p>Nothing was changed.</p>';
			echo '<p><a href="' . $SERVER['PHP_SELF'] . '?id="><img src = "pix/nav/go.png"></a></p>';
		}

	} 
	
	//	if we are not modifying a record we must be creating a new one ...
	//	no need to retrieve any existing values
	else {
		
		$name 		= $_GET['name'];
		$age 		= $_GET['age'];
		$height 	= $_GET['height'];
		$size 		= $_GET['size'];
		$hair 		= $_GET['hair'];
		$eyes 		= $_GET['eyes'];
		$bust 		= $_GET['bust'];
		$outcalls 	= $_GET['outcalls'];
		$comments 	= $_GET['comments'];
			
		echo '<p>Let\'s make a new record<br />$_GET[\'name\'] is : ' . $_GET['name'] . '</p>';
		
		$query = "INSERT INTO `cgirls`.`girls` (`girl_ID`, `on_off`, `name`, `age`, `height`, `size`, `hair`, `eyes`, `bust`, `oc`, `comments`) VALUES (NULL, '', '$name', '$age', '$height', '$size', '$hair', '$eyes', '$bust', '$outcalls', '$comments')";

		$result = wwopq($query);
		if (!$result) {
			die ('Could not query to the database : <br /> ' . mysql_error());
		}
		
		//	haven't checked here that anything has actually been entered
		//	should do that
		echo '<p>You have successfully added a new record.</p>';
		echo '<p><a href="' . $SERVER['PHP_SELF'] . '?id="><img src = "pix/nav/go.png"></a></p>';
	}

}

function sign_on($loc) {
	
	$_GET["id"] = substr($_GET["id"],8);
	if ($loc == 1) {	
		$query = 'UPDATE girls SET on_off = "1" WHERE girl_id = "' . $_GET["id"] . '"';
	} elseif ($loc == 2) {	
		$query = 'UPDATE girls SET on_off = "2" WHERE girl_id = "' . $_GET["id"] . '"';
	}
		$result = wwopq($query);
	if (!$result) {
		die ('Could not query to the database : <br /> ' . mysql_error());
	}
}

function delete_rec() {
	$_GET["id"] = substr($_GET["id"],5);
					
	$query = 'DELETE FROM girls WHERE girl_id = "' . $_GET["id"] . '"';			
	$result = wwopq($query);
	
	echo '<p>The record for id number ' . $_GET["id"] . ' has been deleted.</p><p><a href = "' . $_SERVER['PHP_SELF'] . '?id="><img src = "pix/nav/go.png"></a></p>';
}

function sign_off() {
	$_GET["id"] = substr($_GET["id"],7);			
	$query = 'UPDATE girls SET on_off = "0" WHERE girl_id = "' . $_GET["id"] . '"';	
	$result = wwopq($query);
	if (!$result) {
		die ('Could not query to the database : <br /> ' . mysql_error());
	}

}

function writePst($pstype, $gp) {
	
	// 	After get_gp($girl_id), $gp[$x] contains the urls of the pix in the girl's gallery
	//	(Will have to work out how to deal with thumbnails)
	//	Assuming $gp[$x] contains anything at all, ie: there are some pix in the gallery
	//	this function writes the style section in the head of the document which sets
	//	those pix as the background images in each of the table cells in the photo strip at
	//	the bottom of the page.
	
	//	If this style section is not written then the page reads the style section from the stylesheet
	//	cgirlsstyle.css by default.
	//	Also, if the girl has less than ten photos available, the remainder of the strip defaults
	//	back to the urls defined in the separate stylesheet.
	
	if ($pstype == 'pro') {
		$style = '<style type="text/css">
<!--
';	
		for ($j = 1; $j < 11; $j++) {
			$x = 'fs' . $j;
			if ($gp[$x] == '') {
				//	echo 'Outta here : $gp[$x] is : ' . $gp[$x] . '$j is : ' . $j . '<br />';
				break;
			}	
			else {
			//	added the next line but one to do the css rollover - just trying it out
			$style .= '#pst' . $j . '{ background-image : url(' . $gp[$x] . 'tn.jpg); }
			#pst' . $j . ':hover{ background-image : url(' . $gp[$x] . 'tna.jpg); }
';
			}
	}	
	$style .= '-->
</style>
';
	echo $style;
	return $j - 1;
	}
	elseif ($pstype == 'gen') {
		echo '<style type="text/css">
<!--
#pst1{ background-image : url(' . PST_1_ST . 'tn.jpg); }
#pst1:hover{ background-image : url(' . PST_1_ST . 'tna.jpg); }
#pst2{ background-image : url(' . PST_2_ST . 'tn.jpg); }
#pst2:hover{ background-image : url(' . PST_2_ST . 'tna.jpg); }
#pst3{ background-image : url(' . PST_3_ST . 'tn.jpg); }
#pst3:hover{ background-image : url(' . PST_3_ST . 'tna.jpg); }
#pst4{ background-image : url(' . PST_4_ST . 'tn.jpg); }
#pst4:hover{ background-image : url(' . PST_4_ST . 'tna.jpg); }
#pst5{ background-image : url(' . PST_5_ST . 'tn.jpg); }
#pst5:hover{ background-image : url(' . PST_5_ST . 'tna.jpg); }
#pst6{ background-image : url(' . PST_6_ST . 'tn.jpg); }
#pst6:hover{ background-image : url(' . PST_6_ST . 'tna.jpg); }
#pst7{ background-image : url(' . PST_7_ST . 'tn.jpg); }
#pst7:hover{ background-image : url(' . PST_7_ST . 'tna.jpg); }
#pst8{ background-image : url(' . PST_8_ST . 'tn.jpg); }
#pst8:hover{ background-image : url(' . PST_8_ST . 'tna.jpg); }
#pst9{ background-image : url(' . PST_9_ST . 'tn.jpg); }
#pst9:hover{ background-image : url(' . PST_9_ST . 'tna.jpg); }
#pst10{ background-image : url(' . PST_10_ST . 'tn.jpg); }
#pst10:hover{ background-image : url(' . PST_10_ST . 'tna.jpg); }
-->
</style>';
	}
	elseif ($pstype == 'htnrooms'){
		echo '<style type="text/css">
<!--
#pst1{ background-image : url(pix/rooms/IMG_2398tn.JPG); }
#pst1:hover{ background-image : url(pix/rooms/IMG_2398tna.JPG); }
#pst2{ background-image : url(pix/rooms/IMG_2406tn.jpg); }
#pst2:hover{ background-image : url(pix/rooms/IMG_2406tna.jpg); }
#pst3{ background-image : url(pix/rooms/IMG_2434tn.jpg); }
#pst3:hover{ background-image : url(pix/rooms/IMG_2434tna.jpg); }
#pst4{ background-image : url(pix/rooms/IMG_2435tn.jpg); }
#pst4:hover{ background-image : url(pix/rooms/IMG_2435tna.jpg); }
#pst5{ background-image : url(pix/rooms/IMG_2436tn.jpg); }
#pst5:hover{ background-image : url(pix/rooms/IMG_2436tna.jpg); }
#pst6{ background-image : url(pix/rooms/IMG_2437tn.jpg); }
#pst6:hover{ background-image : url(pix/rooms/IMG_2437tna.jpg); }
#pst7{ background-image : url(pix/rooms/IMG_2439tn.jpg); }
#pst7:hover{ background-image : url(pix/rooms/IMG_2439tna.jpg); }
#pst8{ background-image : url(pix/rooms/IMG_2441tn.jpg); }
#pst8:hover{ background-image : url(pix/rooms/IMG_2441tna.jpg); }
#pst9{ background-image : url(pix/rooms/IMG_2445tn.jpg); }
#pst9:hover{ background-image : url(pix/rooms/IMG_2445tna.jpg); }
#pst10{ background-image : url(pix/rooms/IMG_2446tn.jpg); }
#pst10:hover{ background-image : url(pix/rooms/IMG_2446tna.jpg); }
-->
</style>';
	}
	elseif ($pstype == 'ren'){
		echo '<style type="text/css">
<!--
#pst1{ background-image : url(pix/renov/IMG_2223tn.JPG); }
#pst2{ background-image : url(pix/renov/IMG_2227tn.JPG); }
#pst3{ background-image : url(pix/renov/IMG_2229tn.JPG); }
#pst4{ background-image : url(pix/renov/IMG_2238tn.JPG); }
#pst5{ background-image : url(pix/renov/IMG_2247tn.JPG); }
#pst6{ background-image : url(pix/renov/IMG_2252tn.JPG); }
#pst7{ background-image : url(pix/renov/IMG_2278tn.JPG); }
#pst8{ background-image : url(pix/renov/IMG_2288tn.JPG); }
#pst9{ background-image : url(pix/renov/IMG_2294tn.JPG); }
#pst10{ background-image : url(pix/renov/IMG_2314tn.JPG); }
-->
</style>';
	}
}

function get_gp($girl_id){

	$query = 'select * FROM girls WHERE girl_id = "' . $girl_id . '"';			
	$result = wcallq($query);
	
	while($result_row = mysql_fetch_array($result, MYSQL_ASSOC)) {	
		
		$gp['girl_id']	=	$result_row['girl_ID'];
		$gp['home']		=	$result_row['home'];
		$gp['name']		=  	$result_row['name'];
		$gp['age']		=	$result_row['age'];
		$gp['height']	= 	$result_row['height'];
		$gp['size']		= 	$result_row['size'];
		$gp['hair']		= 	$result_row['hair'];
		$gp['eyes']		= 	$result_row['eyes'];
		$gp['bust']		= 	$result_row['bust'];
		$gp['oc']		= 	$result_row['oc'];
		$gp['comments']	= 	$result_row['comments'];
		$gp['slp']		=	$result_row['slp'];
		$gp['fs1']		=	$result_row['fsa'];
		$gp['fs2']		=	$result_row['fsb'];
		$gp['fs3']		=	$result_row['fsc'];
		$gp['fs4']		=	$result_row['fsd'];
		$gp['fs5']		=	$result_row['fse'];
		$gp['fs6']		=	$result_row['fsf'];
		$gp['fs7']		=	$result_row['fsg'];
		$gp['fs8']		=	$result_row['fsh'];
		$gp['fs9']		=	$result_row['fsi'];
		$gp['fs10']		=	$result_row['fsj'];
	
	}		
	return $gp;
}

function show_edit_list() {
	echo '<h2>Choose a name to view the details'; // , or select <a href="'. $SERVER['PHP_SELF'] . '?id=new">NEW</a></h2>';
	$query = 'SELECT girl_id, name, home FROM girls ORDER BY name';
	$result = wcallq($query);

	$elect =  '<form action = "' . $_SERVER['PHP_SELF'] . '" method = "GET"><select name = "girls">';
	$elect .= '<option class = "largertext" name = "select here">Select here</option>';
	while($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
		$elect .= '<option class = "largertext" name = "sel_g">' . $row['home'] . ':' . $row['name'] . ':' . $row['girl_id'] . '</option>';
	}
	$elect .= '</select><input type = "submit" value = "Go" /></form>';
	echo $elect;	
}
	
function on_shift_list($location) {
	
	//	$location is passed to the function from the calling page
	//	It tells us which location we are dealing with
	
	if ($location != 'edit'){

		// if $location is not 'edit' we are dealing with a place
		
		//if($location == 'Hamilton') {
			//	define query for Hamilton
			//	$loc_id is to be passed to the profile page along with the girl's name
			//	so that we know which location we are in
			//$loc_id = 1;	
			//$query = 'SELECT name, girl_id FROM girls WHERE on_off = 1';
		//}
		//if($location == 'Tauranga') {
			//	define query for Tauranga
			//	$loc_id is to be passed to the profile page along with the girl's name
			//	so that we know which location we are in
			//$loc_id = 2;
			//$query = 'SELECT name, girl_id FROM girls WHERE on_off = 2';
		//}
		//$result = candq($query);
		
		
		
		//	build the who's on box
		date_default_timezone_set('Pacific/Auckland');
		$box	= '<!-- start function on_shift_list($location) -->
	';
		$box 	.= '<div id ="date">
		' . date('D jS \ F h:i A') . '
		<hr>
	</div>
	';
		$box 	.= '<div id = "wo">
		';
		$box	.= '<div class = "medtext">
			Available now
		</div>
		';
		
		//	Hamilton
		$loc_id = 1;
		$query = 'SELECT name, girl_id FROM girls WHERE on_off = 1';
		$result = wcallq($query);
		if ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
			$result = wcallq($query);
			$box	.= '<div class = "wotext">
			<br />
			Hamilton
		</div>
		';
			$box	.= '<div class = "shiftlist">
			';
			while($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
				$box	.= '<a href = "profile.php?id=' . $loc_id . $row['girl_id'] . '">' . $row['name'] . '</a>
			<br />';
			}
			$box	.= '
		</div>
	';
		}
		
		//	Tauranga
		$loc_id = 1;
		$query = 'SELECT name, girl_id FROM girls WHERE on_off = 2';
		$result = wcallq($query);
		if ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
			$result = wcallq($query);
			$box	.= '	<div class = "wotext">
			<br />
			Tauranga
		</div>
		';
			
			$box	.= '<div class = "shiftlist">
			';
			while($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
				$box	.= '<a href = "profile.php?id=' . $loc_id . $row['girl_id'] . '">' . $row['name'] . '</a>
			<br />';
			}
			$box	.= '
		</div>
	';
		}
		
		$box	.= '</div>';
		$box 	.= '
<!-- end function on_shift_list($location) -->
		
';
		//	write the box
		echo $box;

	} else {
		
		//	$location must be 'edit' which means we are serving up 'edit_who.php'
		echo'<div><h2>On now in Hamilton</h2>';
		//	define query for Hamilton
		$query = 'SELECT name, girl_id FROM girls WHERE on_off = 1';
		
		$result = wcallq($query);
		echo '<table border = "0" cellpadding = "3">';
		while($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
			echo '<tr><td>' . $row['name'] . '</td><td class = "xt"><a href="' . $_SERVER['PHP_SELF'] . '?id=signoff' . $row['girl_id'] . '"><img src = "pix/nav/drop.png"></a></td></tr>';
	
		}
		echo '</table>';

		echo'<h2>On now in Tauranga</h2>';
		//	define query for Tauranga
		$query = 'SELECT name, girl_id FROM girls WHERE on_off = 2';

		$result = wcallq($query);
		echo '<table border = "0" cellpadding = "3">';
		while($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
			echo '<tr><td>' . $row['name'] . '</td><td class = "xt"><a href="' . $_SERVER['PHP_SELF'] . '?id=signoff' . $row['girl_id'] . '"><img src = "pix/nav/drop.png"></a></td></tr>';
	
		}
		echo '</table></div>';
	}
}											//	format corrected

function writePromoPic($location, $girl_id) {
	if($location == 'Hamilton'){
		$loc_id = 1;
	}
	elseif ($location == 'Tauranga'){
		$loc_id = 2;
	}
	
	$query = 'SELECT name, slp FROM girls WHERE girl_id = "' . $girl_id . '"';
	$result = wcallq($query);
	while($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
		$name	= $row['name'];
		$slp	= $row['slp'];
	}
	$prpic = '';
	
	if(stristr($_SERVER['PHP_SELF'], 'profile.php')) {
		$prpic	.= '<div id = "top_left_box">';
		if ($slp) {
			$prpic	.=	'Click on the photo strip';
			$prpic .= '<center><img src = "pix/prof/' .  $girl_id . '/' . $girl_id . '100.jpg"></center>';
		}
		else{
			$prpic .= '<p>' . $name . ' does not have any photos yet.</p>';
			
		}
			$prpic .=	'</div>';
	}
	else{
		$prpic	.= '<div id = "top_left_box">';
		$query = 'SELECT name FROM girls WHERE girl_id = "' . PROMO_PIC . '"';
		$result = wcallq($query);
		while($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
			$name	= $row['name'];
		}
		$prpic	.=	'Click below for more of ' . $name;
		$prpic 	.=	'<a href="profile.php?id=' . $loc_id . PROMO_PIC . '"><img src ="pix/prof/' . PROMO_PIC . '/' . PROMO_PIC . '100.jpg"></a>';
		$prpic .=	'</div>';
	
	
	}
	echo $prpic;

}

function sign_on_off_list() {
	
	$query = 'SELECT  name, girl_id FROM girls ORDER BY name';
	$result = wcallq($query);
	
	$f = 1;
	while($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
		$tab[$f]['name'] = $row['name'];
		$tab[$f]['id'] = $row['girl_id'];
		$f++;
	}
	
	$tablex = '';
	$tablex .=	'<table border = "1" width = "100%">
	';
	
	$d = 1;
	for ($i = 1; $i < ($f + 7)/4; $i++){
	
		$tablex .=	'	<tr>';
		
		for ($j = 1; $j < 5; $j++){
			$tablex .=	'
			<td class = "ladiesList"><a href="' . $_SERVER['PHP_SELF'] . '?id=ed_g' . $tab[$d]['id'] . '">' . $tab[$d]['name'] . '</a></td>
			<td class = "ladiesList"><a href="' . $_SERVER['PHP_SELF'] . '?id=signon_h' . $tab[$d]['id'] . '">H</a></td>
			<td class = "ladiesList"><a href="' . $_SERVER['PHP_SELF'] . '?id=signon_t' . $tab[$d]['id'] . '">T</a></td>
		';
			$d++;
		}
		$tablex .=	'</tr>
	';
	}
$tablex .=	'</table>';
	
	echo $tablex;

}

function write_credits() {
	
	echo '<!-- start function write_credits() -->
	<table border = "0">
		<tr>
			<td>
				<div class="credits">&copy; 2009 Orchestral Holdings Ltd  PO BOx 29 Cambridge New Zealand.</div>
				<img src="pix/topimg.gif" width="895" height="133">	
			</td>
		</tr>
	</table>
<!-- end function write_credits() -->

';
}													//	format corrected

function listLadies($location) {
	if($location == 'Hamilton'){
		$loc_id = 1;
	}
	elseif ($location == 'Tauranga'){
		$loc_id = 2;
	}

	
	$query = 'SELECT  name, girl_id, slp FROM girls ORDER BY name';
	$result = wcallq($query);
	
	$f = 1;
	while($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
		$tab[$f]['name'] = $row['name'];
		$tab[$f]['id'] = $row['girl_id'];
		$tab[$f]['slp'] = $row['slp'];
		$f++;
	}
	
	$tablex = '';
	$tablex .=	'<table width = "100%">
	';
	
	$d = 1;
	for ($i = 1; $i < ($f + 7)/4; $i++){
	
		$tablex .=	'<tr>';
		
		for ($j = 1; $j < 5; $j++){
			$tablex .=	'<td class = "ladiesList"><a href="profile.php?id=' . $loc_id . $tab[$d]['id'] . '">' . $tab[$d]['name'];
			if ($tab[$d]['slp']){
				$tablex .=	 '*';
			}
			$tablex .=	 '</a></td>';
			$d++;
		}
		$tablex .=	'</tr>';
	}
	$tablex .=	'</table>';
	
	echo $tablex;

}

function bottomnavStrip($location){
	$ns = '';
	$ns .= '<!-- start function bottomnavStrip($location) -->
	';
	$ns .= '<div id = "bottomnavStrip">
		';
	
	if ($location == "Hamilton") {
		$ns .= '<a href="wkrs.php">Wanker\'s wall</a>
		&nbsp;|&nbsp;
		';
		$ns .= '<a href="htnrenov.php">Renovations</a>
	';
		
		//	taken out blog link until there is one
		// $ns .= '<a href="cgirlsblog.php">Blog</a>
		//&nbsp;
		//';
	
	}
	
	$ns .= '</div>
	';
	$ns .= '<!-- end function bottomnavStrip($location) -->';
	echo $ns;
	
}											//	format corrected

function topnavStrip($location){
	$ns = '';
	$ns .= '<!-- start function bottomnavStrip($location) -->
	';
	$ns .= '<div id = "topnavStrip">
		';
	
	if ($location == "Hamilton") {
		$ns .= '<a href="ladies.php">Ladies</a>
		&nbsp;|&nbsp;
		';
		$ns .= '<a href="loc.php">Location</a>
		&nbsp;|&nbsp;
		';
		$ns .= '<a href="htnrooms.php">Premises</a>
		&nbsp;|&nbsp;
		';
		// taken out sands link until I can work out what to put in here
		//	$ns .= '<a href="sands.php">Spa & Sauna</a>
		//&nbsp;|&nbsp;
		//';
		$ns .= '<a href="prandinfo.php">Rates</a>
		&nbsp;|&nbsp;
		';
		$ns .= '<a href="opportunities.php">Opportunities</a>
		&nbsp;&nbsp;
	';	
		
	}
	
	$ns .= '</div>';
	$ns .= '<!-- start function bottomnavStrip($location) -->
	';
	echo $ns;
	
}											//	format corrected

function phone()	{
	echo '<div id = "phone"><span class = "smbrk"><br /></span>Htn (07) 834 1247<span class = "smbrk"><br /><br /></span>Tga (07) 579 0091</div>';
			}

function write_ad(){
	$num = rand(1, 49);
	
	echo '<div class = "adbox"><img src = "pix/come/' . $num . '.gif"></div>';
	//if(stristr($_SERVER['PHP_SELF'], 'profile.php')) {
	//	echo '<div id ="date"><span class = "smbrk"><br /></span>' . date('D jS \ F h:i A') . '</div>';
	//}
}

function write_nav_box($location) {
	$nb = '';
	$nb .= '<div id = "nav_box">
		<ul>';
	if ($location == 'Hamilton') {
		$nb .= '		<li><a href="pageone.php">Home</a>';
		$nb .= '		<li><a href="ladies.php">Ladies</a>';
		$nb .= '		<li><a href="htnrooms.php">Premises</a>';
		$nb .= '		<li><a href="sands.php">Spa & Sauna</a>';
		$nb .= '		<li><a href="htnrenov.php">Renovations</a>';
		$nb .= '		<li><a href="prandinfo.php">Rates</a>';
		$nb .= '		<li><a href="opportunities.php">Opportunities</a>';
	}
	else {
	
	}
		$nb .= '	</ul>
		</div>';
	
	echo $nb;
}

function write_js(){

	$hjs = '';
	$hjs .= '<script type="text/javascript">
function toggle(div_id) {
	var el = document.getElementById(div_id);
	if ( el.style.display == \'none\' ) {
		el.style.display = \'block\';
	}
	else {
		el.style.display = \'none\';
	}
}

function blanket_size(popUpDivVar) {
	if (typeof window.innerWidth != \'undefined\') {
		viewportheight = window.innerHeight;
	} else {
		viewportheight = document.documentElement.clientHeight;
	}
	if ((viewportheight > document.body.parentNode.scrollHeight) && (viewportheight > document.body.parentNode.clientHeight)) {
		blanket_height = viewportheight;
	} else {
		if (document.body.parentNode.clientHeight > document.body.parentNode.scrollHeight) {
			blanket_height = document.body.parentNode.clientHeight;
		} else {
			blanket_height = document.body.parentNode.scrollHeight;
		}
	}';
	
	$hjs .= 'var blanket = document.getElementById(\'blanket\');
	blanket.style.height = blanket_height + \'px\';
	var popUpDiv = document.getElementById(popUpDivVar);
	popUpDiv_height=blanket_height/2-300;	//150 is half popup\'s height
	popUpDiv.style.top = popUpDiv_height + \'px\';
}

function window_pos(popUpDivVar) {
	if (typeof window.innerWidth != \'undefined\') {
		viewportwidth = window.innerHeight;
	} else {
		viewportwidth = document.documentElement.clientHeight;
	}
	if ((viewportwidth > document.body.parentNode.scrollWidth) && (viewportwidth > document.body.parentNode.clientWidth)) {
		window_width = viewportwidth;
	} else {
		if (document.body.parentNode.clientWidth > document.body.parentNode.scrollWidth) {
			window_width = document.body.parentNode.clientWidth;
		} else {
			window_width = document.body.parentNode.scrollWidth;
		}
	}
	var popUpDiv = document.getElementById(popUpDivVar);
	window_width=window_width/2-250;	//250 is half popup\'s width
	popUpDiv.style.left = window_width + \'px\';
}';

$hjs .= 'function popup(windowname) {
	blanket_size(windowname);
	window_pos(windowname);
	toggle(\'blanket\');
	toggle(windowname);		
}
</script>';

echo $hjs; 
}

function photoStrip($location, $pstype, $girl_id, $gp, $pic_num) {
	if($location == 'Hamilton') {
		$loc_id = 1;
	}
	elseif($location == 'Tauranga') {
		$loc_id = 2;
	}
	//	Must work out here how to decide whether or not to draw the photo strip cell. No point drawing it if there is no picture
	//	Also re-comment this block of code because things have changed since it was done - and all the rest.
	
	//	This draws the photostrip at the bottom of the page.
	//	The thumbnails are set as the background image of each table cell by the style section.
	//	All that is decided here is where to link to from each cell, and the picture to link to
	//	which is actually the full version of the thumbnail
	//	The url of the big picture is contained in $gp[$x] which is passed to the profile page
	// in the "id" along with the girl's id.
	
	if ($pstype == 'pro') {
		$pst = '<!-- start photostrip -->
		<div class = "fs">

		<table border = "1" id = "pst">
			<tr>';
				for($i = 1; $i <= $pic_num; $i++) {
					$x = 'fs' . $i;
					$pst .= '
					<td id = "pst' . $i . '"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv' . $i . '" style="display:none; 	color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv' . $i . '\')"><img src="pix/prof/' . $girl_id . '/' . $girl_id . $i . '.jpg"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#" onclick="popup(\'popUpDiv' . $i . '\')"><img src = "pix/nav/fs.gif"></a></td>
					';
				
				
				
				}	
				$pst .= '</tr>
		</table>
		</div>
		<!-- end photostrip -->
	
	';
	}
	elseif ($pstype == 'gen') {
		
		$pst = '<!-- start photostrip -->
	<div class = "fs">
		<table border = "1" id = "pst">
			<tr>
				<td id = "pst1"><a href="profile.php?id=' . $loc_id . PST_1_PS . '"><img src = "pix/nav/fs.gif"></a></td>
					
				<td id = "pst2"><a href="profile.php?id=' . $loc_id . PST_2_PS . '"><img src = "pix/nav/fs.gif"></a></td>
					
				<td id = "pst3"><a href="profile.php?id=' . $loc_id . PST_3_PS . '"<img src = "pix/nav/fs.gif"></a></td>
					
				<td id = "pst4"><a href="profile.php?id=' . $loc_id . PST_4_PS . '"><img src = "pix/nav/fs.gif"></a></td>
					
				<td id = "pst5"><a href="profile.php?id=' . $loc_id . PST_5_PS . '"><img src = "pix/nav/fs.gif"></a></td>
					
				<td id = "pst6"><a href="profile.php?id=' . $loc_id . PST_6_PS . '"><img src = "pix/nav/fs.gif"></a></td>
					
				<td id = "pst7"><a href="profile.php?id=' . $loc_id . PST_7_PS . '"><img src = "pix/nav/fs.gif"></a></td>
					
				<td id = "pst8"><a href="profile.php?id=' . $loc_id . PST_8_PS . '"><img src = "pix/nav/fs.gif"></a></td>
					
				<td id = "pst9"><a href="profile.php?id=' . $loc_id . PST_9_PS . '"<img src = "pix/nav/fs.gif"></a></a></td>
					
				<td id = "pst10"><a href="profile.php?id=' . $loc_id . PST_10_PS . '"><img src = "pix/nav/fs.gif"></a></td>
			</tr>
		</table>
	</div>
	<!-- end photostrip -->';
	
	}
	elseif ($pstype == 'htnrooms'){
		$pst = '

	<!-- start photostrip -->
		<div class = "fs">

		<table border = "1" id = "pst">
			<tr>
					<td id = "pst1"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv1" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv1\')"><img src="pix/rooms/IMG_2398.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#" onclick="popup(\'popUpDiv1\')"><img src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst2"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv2" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv2\')"><img src="pix/rooms/IMG_2406.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#" onclick="popup(\'popUpDiv2\')"><img src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst3"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv3" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv3\')"><img src="pix/rooms/IMG_2434.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#" onclick="popup(\'popUpDiv3\')"><img src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst4"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv4" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv4\')"><img src="pix/rooms/IMG_2435.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#" onclick="popup(\'popUpDiv4\')"><img src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst5"><div id ="blanket" style="display:none;style="display:none;">
					</div>
					<div id ="popUpDiv5" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv5\')"><img src="pix/rooms/IMG_2436.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#" onclick="popup(\'popUpDiv5\')"><img src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst6"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv6" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv6\')"><img src="pix/rooms/IMG_2437.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#" onclick="popup(\'popUpDiv6\')"><img src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst7"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv7" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv7\')"><img src="pix/rooms/IMG_2439.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#" onclick="popup(\'popUpDiv7\')"><img src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst8"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv8" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv8\')"><img src="pix/rooms/IMG_2441.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#" onclick="popup(\'popUpDiv8\')"><img src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst9"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv9" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv9\')"><img src="pix/rooms/IMG_2445.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#" onclick="popup(\'popUpDiv9\')"><img src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst10"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv10" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv10\')"><img src="pix/rooms/IMG_2446.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#" onclick="popup(\'popUpDiv10\')"><img src = "pix/nav/fs.gif"></a></td>
					</tr>
		</table>
	</div>
	<!-- end photostrip -->	
';

	}
	
	elseif ($pstype == 'ren'){
		$pst = '<!-- start photostrip -->
		<div class = "fs">

		<table border = "1" id = "pst">
			<tr>
				<td id = "pst1"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv1" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv1\')"><img src="pix/renov/IMG_2223.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#" onclick="popup(\'popUpDiv1\')"><img src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst2"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv2" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv2\')"><img src="pix/renov/IMG_2227.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#" onclick="popup(\'popUpDiv2\')"><img src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst3"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv3" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv3\')"><img src="pix/renov/IMG_2229.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#" onclick="popup(\'popUpDiv3\')"><img src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst4"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv4" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv4\')"><img src="pix/renov/IMG_2238.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#" onclick="popup(\'popUpDiv4\')"><img src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst5"><div id ="blanket" style="display:none;style="display:none;">
					</div>
					<div id ="popUpDiv5" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv5\')"><img src="pix/renov/IMG_2247.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#" onclick="popup(\'popUpDiv5\')"><img src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst6"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv6" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv6\')"><img src="pix/renov/IMG_2252.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>&nbsp;&nbsp;&nbsp;There goes Room 6!
					</div>
					<a href="#" onclick="popup(\'popUpDiv6\')"><img src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst7"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv7" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv7\')"><img src="pix/renov/IMG_2278.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#" onclick="popup(\'popUpDiv7\')"><img src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst8"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv8" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv8\')"><img src="pix/renov/IMG_2288.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#" onclick="popup(\'popUpDiv8\')"><img src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst9"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv9" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv9\')"><img src="pix/renov/IMG_2294.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>
					</div>
					<a href="#" onclick="popup(\'popUpDiv9\')"><img src = "pix/nav/fs.gif"></a></td>
					
					<td id = "pst10"><div id ="blanket" style="display:none;">
					</div>
					<div id ="popUpDiv10" style="display:none; color : white; padding : 10px; text-align : center; position : absolute; #top : 233px; #left : 0px; background-color : #333339; #width : 400px; #height : 500px; z-index : 9002; border : ridge blue 4px;">
					<a href="#" onclick="popup(\'popUpDiv10\')"><img src="pix/renov/IMG_2314.JPG"><br /><span class = "clospic">Click on the picture to close it</span></a>&nbsp;&nbsp;&nbsp;Now what do we do?
					</div>
					<a href="#" onclick="popup(\'popUpDiv10\')"><img src = "pix/nav/fs.gif"></a></td>
			</tr>
		</table>
	</div>
	<!-- end photostrip -->';
	}
	
	echo $pst;
}			

?>
