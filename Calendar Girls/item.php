<?php
session_start();
require_once('includes/db_worx.php');
require_once('includes/cgops.php');
require_once('includes/formhelpers.php');
$logged_in = 0;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title>Calendar Girls - Operator log in</title>
<link rel=stylesheet type="text/css" href="style/cgirlsstyle.css">

</head>
<body onload="document.login.username.focus()">

<div align="left" class="mainbox">
	<table border = "1" width = 100%>
		<tr>
			<td>
				<?php
					
					if(!$_SESSION['username']) {		//	If the user is not logged in
														//	Just give them a link to the log in page	
						echo	'<p class = "centre_cell"><a href="log_in.php">Log in</a></p>';
					}
					else{
						menubox();
						$shift_date_code = $_SESSION['$shift_date_code'];


				?>
			</td>
		</tr>
	</table>
	
		<?php
			if($_GET['itemsub']){
				echo '<p>The form has been submitted</p>';
				
				$_SESSION['shift_code'] = '20100618';
				
				echo '<p>$_SESSION[\'first_name\'] is : ' . $_SESSION['first_name'] . '<br />
				$_SESSION[\'shift_code\'] is : ' . $_SESSION['shift_code'] . '</p>';
				//	Get the admin details
				$admin = get_admin();
				
				// check that *ALL* the fields have been entered correctly
				//	if not - send them back to do it again
				
				//	check for tip or cash out on credit card
				//	if it is - work out the handling fee
				
				
				// We do not do cash out on credit card
				
				if(($_GET["item"] == 'TIP' OR $_GET["item"] == 'CASH')){
				
					if ($_GET['credit']){
						echo'<p>We are dealing with a tip or cash out on credit card and will have to deal with it accordingly.<br />';

							$amnt = $_GET["credit"];

							echo '<br />The amount on credit card is : ' . number_format($amnt, 2) . '<br />';
							echo '<br />The tip handling fee is : ' . number_format($admin['tip_hdlfee_c'], 2) . ' per cent, rounded up to the nearest ' . number_format($admin['thf_rounded_up_to'], 2) . ' dollars. <br />';
							$thf = ($amnt / 100) * $admin['tip_hdlfee_c'];
						
							echo '<br />At this point $thf  is : ' . number_format($thf, 2) . ' dollars.<br />';
						
							if($thf < $admin['thf_rounded_up_to']){
								$thf = $admin['thf_rounded_up_to'];
							}
						
							echo '<br />And at this point $thf  is : ' . number_format($thf, 2) . ' dollars.<br />';
						
							$f = fmod($thf, $admin['thf_rounded_up_to']);
						
							echo '<br />The tip modulo the rounding amount is currently : ' . number_format($f, 2) . '<br />';
						
							if($f){
								$add_on = $admin['thf_rounded_up_to'] - $f;
							}
							else {
								$add_on = 0;
							}
						
							echo '<br />So we need to add : ' . number_format($add_on, 2)  . ' to ' . number_format($thf, 2) . ' to bring it up to ' . number_format(($admin['thf_rounded_up_to'] - $f + $thf), 2) . '<br />';
							
							$thf += $add_on;
						
							echo '<br />So the fee in this case is : ' . number_format($thf, 2) . ' dollars.<br />';
							
							$tip_cash += ($amnt - $thf);
							
							
							echo '<br />And the amount paid out will be : ' . number_format($tip_cash, 2) . ' dollars.<br />';
						}
						
						// add EFT amount
						if ($_GET['eft']){
							$eft = $pay_out = $_GET['eft'];
							echo '<br />EFT  is : ' . $eft . ' dollars.<br />';
							$tip_cash += $eft;
							
							echo '<br />Tip total is now  : ' . number_format($tip_cash, 2) . ' dollars.<br />';
							
						}
						
						//	add CASH amount
						if ($_GET['cash']){
							$cash = $pay_out = $_GET['cash'];
							echo '<br />Cash  is : ' . $cash . ' dollars.<br />';
							$tip_cash += $cash;
							
							echo '<br />Tip total is now  : ' . number_format($tip_cash, 2) . ' dollars.<br />';
						}
						
						echo '<br />Tip total ends up as  : ' .  number_format($tip_cash, 2) . ' dollars.<br />';
						$pay = $tip_cash;
						
				}
				
				
				//	once we have worked everything out we will have to enter the record
				

				
				$type = 'ite';
				$service_desc = $_GET["item"];
				$by = $_SESSION['first_name'];
				
				
				$query = "INSERT INTO `cgirls`.`jobs` (`job_id`, `type`, `service_desc`, `shift_date`, `adminref`, `hour`, `minute`, `girl_id`, `cash`, `eft`, `credit`, `pay_out`) VALUES (NULL, '$type', '$service_desc', '$shift_date_code', '1', '$hour', '$minute', '$girl_id', '$cash', '$eft', '$credit', '$pay');";
							
				$result = wwopq($query);
				if (!$result) {
					die ('Could not query to the database : <br /> ' . mysql_error());
				}
				else {
					echo '<p>Job entered successfully</p>';
								
								
					
					write_log('By : ' . $by . ' |  Shift date_code : ' . $shift_date_code . ' | Type : ' . $type . ' | Service_des : ' . $service_desc . ' | Time : ' . $hour . ':' . $minute . ' | Girl_id : ' . $girl_id . ' | Girl_name : ' . $girlsname . ' | Cash : ' . $cash . ' | EFT : ' . $eft . ' | Credit : ' . $credit . ' | Pay_out : ' . $pay);

				}
							
				//	Initialize the girl_id to avoid re-entering the record by mistake
				$girl_id = '';
				

				
				
				
			}
			
			else {
		
		?>
		<form action="item.php" method="GET">
	
		<select name = "girl_id">
			echo '<option value = "house" selected>House</option>';
			
			<?php
				if($_GET["item"] == 'TIP' OR $_GET["item"] == 'DF' OR $_GET["item"] == 'SUB' OR $_GET["item"] == 'CASH' OR $_GET["item"] == 'CDM' OR $_GET["item"] == 'LBE' OR $_GET["item"] == 'SPE'){
					
					$gp = get_gp($_GET['girl_id']);
					
					 
					 echo '<option value = "' . $_GET['girl_id'] . '" selected>' . $gp['name'] . '</option>';
					 
				}
				
				else {
					echo '<option value selected>Select</option>';
				}
				$query = 'SELECT name, girl_id FROM girls WHERE on_off = 1';
				$result = wcallq($query);
				if ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
					$result = wcallq($query);
						while($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
						echo '<option value="' . $row['girl_id'] . '">' . $row['name'] . '</option>';
					}

				}
			?>
		</select>
		
		<br>
		<br>
		

		<select name ="item" onchange="this.form.submit();">
			<?php
					if($_GET["item"] == 'TIP' OR $_GET["item"] == 'DF' OR $_GET["item"] == 'SUB' OR $_GET["item"] == 'CASH' OR $_GET["item"] == 'CDM' OR $_GET["item"] == 'LBE' OR $_GET["item"] == 'SPE'){
				echo '<option value="' . $_GET['item'] . '" selected>' . $_GET['item'] . '</option>';
			}
			else {
				echo '<option value selected>Select</option>';
			}
		?>
		<option value="CDM">Pack condoms</option>
		<option value="LBE">Tube lube</option>
		<option value="SPE">Sponge</option>
		<option value="TIP">Tip</option>
		<option value="DF">Driver's fee</option>
		<option value="SUB">Sub</option>
		<option value="CASH=">Cash out</option>
		</select>
		
		<br>
		<br>
		<?php
			if($_GET["item"] == 'TIP' OR $_GET["item"] == 'DF' OR $_GET["item"] == 'SUB' OR $_GET["item"] == 'CASH'){
		
			
		 echo '<label>Cash<input type = "text" name = "cash" value = "0" /></label>
		
		<br>
		<br>
		
		<label>EFT<input type = "text" name = "eft" value = "0" /></label>
		

		<br>
		<br>
		
		<!--
		//	must deal with credit card tips here
		//	5% rounded up to the nearest $5
		-->
		<label>Credit<input type = "text" name = "credit" value = "0" /></label>
		';
		}

		?>
		<br>
		<br>
		<p>Time : 
		<select name ="hour">
		<option value selected>Hour</option>
		<option value="10">10</option>
		<option value="11">11</option>
		<option value="12">12</option>
		<option value="13">13</option>
		<option value="14">14</option>
		<option value="15">15</option>
		<option value="16">16</option>
		<option value="17">17</option>
		<option value="18">18</option>
		<option value="19">19</option>
		<option value="20">20</option>
		<option value="21">21</option>
		<option value="22">22</option>
		<option value="23">23</option>
		<option value="00">00</option>
		<option value="01">01</option>
		<option value="02">02</option>
		<option value="03">03</option>
		<option value="04">04</option>
		<option value="05">05</option>
		<option value="06">06</option>
		<option value="07">07</option>
		<option value="08">08</option>
		<option value="09">09</option>
		</select>
		
		<select name ="minute">
		<option value selected>Minutes</option>
		<option value="00">00</option>
		<option value="05">05</option>
		<option value="10">10</option>
		<option value="15">15</option>
		<option value="20">20</option>
		<option value="25">25</option>
		<option value="30">30</option>
		<option value="35">35</option>
		<option value="40">40</option>
		<option value="45">45</option>
		<option value="50">50</option>
		<option value="55">55</option>
		</select>
		</p>
		
		<br>
		
		<input type="submit" value="Go" name="itemsub"">
		</form> 
		


<?php
					}
		}
		write_credits();
	?>
</div>
</body>
</html>