<?php
require_once('includes/db_worx.php');
require_once('includes/cgops.php');
require_once('includes/cgdefs.php');
$location = 'Hamilton';
//	set the photostrip type 'profile' or 'generic'
$pstype = 'gen';

// get the $girl_id from the $_GET["id"]
$girl_id = substr($_GET["id"],1);

//	get_gp gets all the information on the girl from the db
//	 puts it into the array $gp
$gp = get_gp($girl_id);

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title>Calendar Girls Hamilton</title>
<link rel=stylesheet type="text/css" href="style/cgirlsstyle.css">

<script type="text/javascript">
function toggle(div_id) {
	var el = document.getElementById(div_id);
	if ( el.style.display == 'none' ) {
		el.style.display = 'block';
	}
	else {
		el.style.display = 'none';
	}
}

function blanket_size(popUpDivVar) {
	if (typeof window.innerWidth != 'undefined') {
		viewportheight = window.innerHeight;
	} else {
		viewportheight = document.documentElement.clientHeight;
	}
	if ((viewportheight > document.body.parentNode.scrollHeight) && (viewportheight > document.body.parentNode.clientHeight)) {
		blanket_height = viewportheight;
	} else {
		if (document.body.parentNode.clientHeight > document.body.parentNode.scrollHeight) {
			blanket_height = document.body.parentNode.clientHeight;
		} else {
			blanket_height = document.body.parentNode.scrollHeight;
		}
	}var blanket = document.getElementById('blanket');
	blanket.style.height = blanket_height + 'px';
	var popUpDiv = document.getElementById(popUpDivVar);
	popUpDiv_height=blanket_height/2-300;	//150 is half popup's height
	popUpDiv.style.top = popUpDiv_height + 'px';
}

function window_pos(popUpDivVar) {
	if (typeof window.innerWidth != 'undefined') {
		viewportwidth = window.innerHeight;
	} else {
		viewportwidth = document.documentElement.clientHeight;
	}
	if ((viewportwidth > document.body.parentNode.scrollWidth) && (viewportwidth > document.body.parentNode.clientWidth)) {
		window_width = viewportwidth;
	} else {
		if (document.body.parentNode.clientWidth > document.body.parentNode.scrollWidth) {
			window_width = document.body.parentNode.clientWidth;
		} else {
			window_width = document.body.parentNode.scrollWidth;
		}
	}
	var popUpDiv = document.getElementById(popUpDivVar);
	window_width=window_width/2-250;	//250 is half popup's width
	popUpDiv.style.left = window_width + 'px';
}function popup(windowname) {
	blanket_size(windowname);
	window_pos(windowname);
	toggle('blanket');
	toggle(windowname);		
}
</script>
<?php
	writePst($pstype, $gp);
?>
</head>
<body>
<div align="left" class="mainbox">
<?php
	photoStrip($location, $pstype, $girl_id, $gp, $pic_num);
?>
<table id = "mt" border = "1">
		<tr>
			<td valign = "top">
				<?php
					writePromoPic($location, $girl_id);
				?>
				<?php
					phone();
					write_ad();
				?>
			<td valign = "top">
				<div class="venuebox">
				
					<?php
						topnavStrip($location);
					?>
					<div class = "venuetext">
				<h2>Rates</h2>
					<p>
					The rates are <ul><li> 30 mins : $140</li>
					<li>45 mins : $160</li>
					<li>60 mins : $180</li>
					<li>Straight double : $360</li>
					<li>Bi-double : $440</li>
					</ul>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;...&nbsp;or multiples thereof
					<br /><br />
					Calendar Girls promotes safe sex - condoms will be used at all times.
					
					</p>
					</div>	



				</div>
				<?php
					bottomnavStrip($location);
				?>
			</td>
			<td valign = "top">
				
				<!-- start who's on box -->	
				<?php
				//	create the list of girls who are on shift
				on_shift_list($location);
				?>
				<!-- end who's on box -->

			</td>
		</tr>
	</table>
	<?php
		write_credits();
	?>
</div>
</body>
</html>