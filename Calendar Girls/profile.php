<?php
require_once('includes/db_worx.php');
require_once('includes/cgops.php');
require_once('includes/cgdefs.php');

$location = 'Hamilton';
	if (strstr($_GET["id"], 'preview')){
		$_GET["id"] = '1' . substr($_GET["id"],7);
		$preview = 1;
		// echo '<p>It says PREVIEW - $_GET["id"] is now : ' . $_GET["id"] . '</p>';
	}
// get the $girl_id from the $_GET["id"]
$girl_id 	=	substr($_GET["id"],1);
// $girl_id 	=	$_GET["id"];
//	set the photostrip type 'profile' or 'generic'
$pstype = 'pro';

//	get_gp gets all the information on the girl from the db
//	 puts it into the array $gp
$gp = get_gp($girl_id);

echo DOCTYPE;
echo HTML_START . HEAD_START . TITLE_START . TITLE . TITLE_END . STYLE_LOC . JS_LOC;

//	write the javascript to show the pics in the photostrip
//	write_js();

//	 if $gp['has_pix'] is set - indicates the availability of girl's own photos
//	the pics in the film strip will be set from here
//	otherwise they are set by the style rules in the separate style sheet.


	
if ($gp['has_pix']){
	$pic_num = 	writePst($pstype, $gp);
}
echo HEAD_END;
?>
<body>
<div align="left" class = "mainbox">
<?php
	photoStrip($location, $pstype, $girl_id, $gp, $pic_num, $preview);
?>
	<table id = "mt" border = "1">
		<tr>
			<td valign = "top">
				<?php
					writePromoPic($girl_id);
					phone();
					write_ad();	
				?>	
			</td>
			
			
			
			<!-- ********  Start centre box ********  -->
			<td valign = "top">
				<div class="venuebox">
					
					<?php
						topnavStrip($location);
						buildViewGirl($gp, $location);
						
						if($preview != 0){
							
							echo '<br /><br /><hr><p class = "preview">This is a PREVIEW of ' . $gp['name'] . '\'s web page<br /><br /><a href="edit_who.php?id=ed_g' . $gp["girl_id"] . '">BACK</a></p>';
						}
						?>
				</div>
				<?php
					bottomnavStrip($location);
				?>
			</td>
			<!-- ********   End  centre box ********  -->
			
			
			
			<td valign = "top">
				<?php
				on_shift_list($location);
				?>
			</td>
		</tr>
	</table>
	<?php
		write_credits();
	?>
</div>
</body>
</html>