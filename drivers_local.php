<?php
require_once('includes/db_worx.php');
require_once('includes/cgops.php');
require_once('includes/cgdefs.php');
$location = 'Hamilton';
//	set the photostrip type 'profile' or 'generic'
$pstype = 'gen';
echo DOCTYPE;
echo HTML_START . HEAD_START . TITLE_START . TITLE . ' ' . $location . TITLE_END . STYLE_LOC . JS_LOC;


writePst($pstype, $gp);

echo HEAD_END;
?>
<body>
<div align="left" class = "mainbox">
<?php
	photoStrip($location, $pstype, $girl_id, $gp, $pic_num);
?>
	<table id = "mt" border = "1">
		<tr>
			<td valign = "top">
				<?php
					writePromoPic($location, $girl_id);
					phone();
					write_ad();	
				?>	
			</td>
			
			
			
			<!-- ********  Start centre box ********  -->
			<td valign = "top">
				<div class="venueboxladies">
				
					<?php
						topnavStrip($location);
					?>
					<div class = "venuetext">Drivers' fees</div>

<table border = 1>
<tr><th>No. of hours</th><th>Total cost</th><th>Driver</th></th></tr>
<tr><td>1</td><td>210</td><td>30</td></tr>
<tr><td>2</td><td>410</td><td>50</td></tr>
<tr><td>3</td><td>610</td><td>70</td></tr>
<tr><td>4</td><td>800</td><td>80</td></tr>
<tr><td>5</td><td>1000</td><td>100</td></tr>
<tr><td>6</td><td>1180</td><td>100</td></tr>
<tr><td>7</td><td>1360</td><td>100</td></tr>
<tr><td>8</td><td>1540</td><td>100</td></tr>
<tr><td>9</td><td>1720</td><td>100</td></tr>
<tr><td>10</td><td>1900</td><td>100</td></tr>

</table>

</div>
				<?php
					bottomnavStrip($location);
				?>
			</td>
			<!-- ********   End  centre box ********  -->
			
			
			
			<td valign = "top">
				<?php
				on_shift_list($location);
				?>
			</td>
		</tr>
	</table>
	
	<hr class = "picshop_hr">
	
	<!--
	<script type="text/javascript">
	getImgSize();
	</script>
	-->
	<?php
		write_credits();
	?>
</div>
</body>
</html>