<?php

session_start();
require_once('includes/db_worx.php');
require_once('includes/cgops.php');
require_once('includes/formhelpers.php');
$on_shift_list = 0;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title>Calendar Girls</title>
<link rel=stylesheet type="text/css" href="style/cgirlsstyle.css">

</head>
<body>

<div align="left" class="mainbox">
	<?php
		if(!$_SESSION['username']) {		//	If the user is not logged in
											//	Just give them a link to the log in page	
			echo	'<p class = "centre_cell"><a href="log_in.php">Log in</a></p>';
		}
		else{		//	Otherwise - carry on ...
			menubox();
			?>
			<table border = "2" width = "100%">
				<tr>
					<td>
						<div class = "centre_cell">
							<?php
								//	This is where we decide what we are going to do on this page

								if ($_GET['id'] == 'new'){
									$new = 1;
								} elseif (strstr($_GET["id"], 'ed_g')){
									$new = 2;
									$girl_id = substr($_GET["id"],4);
								} else {
									$new = 0;
								}

								if($new){
									//	We want to enter a new record or edit an existing one
									buildEditForm($new, $girl_id);
								}

								elseif($_GET["girls"]) {
						
									//	We want to view a girl's details
									// Get the girl_id and call get_gp() which gets all the details for that girl_id
									$pat  = '/:[0-9]+$/';
									preg_match($pat, $_GET["girls"], $match);
									$girl_id = substr($match[0],1);
									$gp = get_gp($girl_id);

									//	present the results
									buildViewGirl($gp, $location);
								}

								elseif($_GET['editform']) {
									//	The form has been submitted - insert the new values into the database
									upd_girls_from_form();
								}

								elseif(strstr($_GET["id"], 'del_g')) {

									//	We are going to delete a record			
									delete_rec();
								}
	
								elseif(strstr($_GET["id"], 'signon')) {
									//	sign on in Hamilton
									if (strstr($_GET["id"], 'signon_h')) {
										$loc = 1;
									}
									if (strstr($_GET["id"], 'signon_t')) {
									//	Sign on in Tauranga
										$loc = 2;
									}

									//	Girl has arrived	
									sign_on($loc);
								}

								elseif(strstr($_GET["id"], 'signoff')) {
									//	Girl has gone home	
									sign_off();
								}
								
					
								// 	present the list of girls and sign on ticks
								//	and the instructions
								if ($new === 0) {
																	echo '<div class = "xtx">
	Hi Tracey !
	<br />
	Click on \'H\'<!-- for Hamilton, or \'T\' for Tauranga--> to put the girls ON SHIFT
	<br />
	Click on the red cross to take them off.
	';
								echo '<br />
	Click on the girl\'s name to edit her details.
	<br />
	Click on <a href="' . $_SERVER['PHP_SELF'] . '?id=new">NEW</a> to create a new record.<br /><br />
</div>
';
									sign_on_off_list($loc_id);
									$on_shift_list = 1; 
								}
								
							echo '</div></td>
							<td valign = "top">';

			}
						$location = 'edit';
						//	create the list of girls who are on shift
				
						if($on_shift_list){
							on_shift_list($location);
						}
						echo '</td></tr></table>';
					?>


	<?php
		write_credits();
	?>

</body>
</html>