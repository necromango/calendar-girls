<?php
//	definitions - if I can think of any.

//	for the photostrip section - girl_id number
define("PST_1_PS", "30");	//	Kitty
define("PST_2_PS", "24");	//	Sarah
define("PST_3_PS", "28");	//	Porscha
define("PST_4_PS", "30");	//	Kitty
define("PST_5_PS", "41");	//	Cleo
define("PST_6_PS", "70");	//	Summer
define("PST_7_PS", "76");	//	Aria
define("PST_8_PS", "22");	//	Anya
define("PST_9_PS", "28");	//	Porscha
define("PST_10_PS", "62");	//	Cassie

define("PIXPROF", "pix/prof/");
define("MAIN_PIC", "100");
define("PIC_SFX", ".jpg");

// for the style section - the photo number
define("PST_1_ST", PIXPROF . PST_1_PS . "/" . PST_1_PS . "2");
define("PST_2_ST", PIXPROF . PST_2_PS . "/" . PST_2_PS . "1");
define("PST_3_ST", PIXPROF . PST_3_PS . "/" . PST_3_PS . "9");
define("PST_4_ST", PIXPROF . PST_4_PS . "/" . PST_4_PS . "3");
define("PST_5_ST", PIXPROF . PST_5_PS . "/" . PST_5_PS . "1");
define("PST_6_ST", PIXPROF . PST_6_PS . "/" . PST_6_PS . "4");
define("PST_7_ST", PIXPROF . PST_7_PS . "/" . PST_7_PS . "3");
define("PST_8_ST", PIXPROF . PST_8_PS . "/" . PST_8_PS . "7");
define("PST_9_ST", PIXPROF . PST_9_PS . "/" . PST_9_PS . "10");
define("PST_10_ST", PIXPROF . PST_10_PS . "/" . PST_10_PS . "2");

// picture to go top left - featured girl
define ("PROMO_PIC", "28");	//	Porscha


define("DOCTYPE", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"
        \"http://www.w3.org/TR/html4/loose.dtd\">");
define("HTML_START", "<html>");
define("HTML_END", "</html>");
define("HEAD_START", "<head>");
define("HEAD_END", "</head>");
define("TITLE_START", "<title>");
define("TITLE_END", "</title>");
define("ICO", "
<link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"/favicon.ico\">");
define("STYLE_LOC", "
<link rel=stylesheet type=\"text/css\" href=\"style/cgirlsstyle.css\">");
define("JS_LOC", "
<script type=\"text/javascript\" src =\"js/cg.js\"></script>");

define("TITLE", "Calendar Girls");

// file locations
define("LOGIN", "log_in.php");
define("ED_SHIFT", "edit_who.php");
define("JOB", "job.php");

// picshop stuff
define("MAXIM", 10);
define("PMAXDIM", 500);
define("MAX_TN_DIM", 85);
define("TNFAC", PMAXDIM / MAX_TN_DIM);
define("FS", "pix/nav/fs.gif");
define("FSA", "pix/nav/fsa.gif");
?>
