<?php
session_start();
require_once('includes/db_worx.php');
require_once('includes/cgops.php');
require_once('includes/formhelpers.php');
$logged_in = 0;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title>Calendar Girls - Operator log in</title>
<link rel=stylesheet type="text/css" href="style/cgirlsstyle.css">

</head>
<body onload="document.login.username.focus()">

<div align="left" class="mainbox">
	<table border = "1" width = 100%>
		<tr>
			<td>
				<?php
					
					if(!$_SESSION['username']) {		
					//	If the user is not logged in
					//	Just give them a link to the log in page
					
						echo	'<p class = "centre_cell"><a href="log_in.php">Log in</a></p>';
					}
					else {
					// Show the menu box
						menubox();
						
						
						//	testing $_SESSION['shift_code']
						
						echo '<p>$_SESSION[\'shift_code\'] is : ' . $_SESSION['shift_code'] . '</p>';
						//	If the job form has been submitted 
						//	we need to process the input

						if($_GET["submit"]){
							
							//	Get the admin details
							$admin = get_admin();
							
							//	Get the girl's name
							$gp = get_gp($_GET["girl_id"]);
							
							//	Set $job_errors to 0
							$job_errors = 0;
							
							//	Set $box_status to OK
							$box_status = 1;
							$note 		= 'OK';
							
							//	Add up cash, eft, and credit
							$total_paid = $_GET["cash"] + $_GET["eft"] + $_GET["credit"];
							
							
							echo '<hr><p class="medtext">You have entered the following details : Please check before confirming.</p>';
							
							
							echo '<table class = "check_booking"><tr>';
							
							
							// Check that there is a girl's name
							// If not, add 1 to $job_errors
							if(!$gp['name']){
								$box_status = 0;
								$job_errors += 1;
								$note = 'There\'s no name';
								
							}
							
							echo '<td class ="check_booking_cell_left">Name</td><td class="check_booking_cell_' . $box_status . '">' . $gp['name'] . ' </td><td class="check_booking_cell_note">' . $note . '</td></tr>';
							
							//	Check that a service has been entered
							$box_status = 1;
							//	$note 		= 'OK';
							if(!$_GET["service"]){
								$box_status = 0;
								$job_errors += 1;
								$note = 'Nothing entered';
								
							}
						
							echo '<tr><td class ="check_booking_cell_left">Service</td><td class="check_booking_cell_' . $box_status . '">'  . $_GET["service"] . '</td><td class="check_booking_cell_note">' . $note . '</td></tr>';
							
							$box_status = 1;
							$note 		= 'OK';
							

							if ($_GET["cash"] > 0){
								echo '<tr><td class ="check_booking_cell_left">Cash</td><td class="check_booking_cell_' . $box_status . '">'  . $_GET["cash"] . '</td><td class="check_booking_cell_note"></td></tr>';
							}
							
						
							
							$box_status = 1;
							$note 		= 'OK';
								
							if ($_GET["eft"] > 0){
								echo '<tr><td class ="check_booking_cell_left">EFT</td><td class="check_booking_cell_' . $box_status . '">'  . $_GET["eft"] . '</td><td class="check_booking_cell_note"></td></tr>';
							}
							
							$box_status = 1;
							$note 		= 'OK';

							if ($_GET["credit"] > 0){
								echo '<tr><td class ="check_booking_cell_left">Credit</td><td class="check_booking_cell_' . $box_status . '">'  . $_GET["credit"] . '</td><td class="check_booking_cell_note"></td></tr>';
							}
							
							
							$box_status = 1;
							$note 		= 'OK';
								
							if($total_paid != $admin['price_' . $_GET["service"]]){
								$box_status = 0;
								$job_errors += 1;
								$note = 'Should be ' . $admin['price_' . $_GET["service"]];
								if($_GET["service"] == '60' && $total_paid == $admin['price_' . $_GET["service"]] - 20 && $_GET["disc"] == 1) {
									$box_status = 1;
									$note 		= 'OK';
									$job_errors -= 1;
								}
							}
							if($total_paid == 0){
								$box_status = 0;
								$job_errors += 1;
								$note = 'Nothing is free';
								
							}
							
							echo '<tr><td class ="check_booking_cell_left">Total paid</td><td class="check_booking_cell_' . $box_status . '">' . $total_paid . '</td><td class="check_booking_cell_note">' . $note . '</td></tr>';
							
							
							$box_status = 1;
							$note 		= 'OK';
								
							if(!$_GET['hour'] or !$_GET['minute']){
								$box_status = 0;
								$job_errors += 1;
								$note = 'Your time is wrong';
							}
							else{
								if ($_GET['hour'] > 9 && $_GET['hour'] < 19){
									$note = 'Day';
								}
								else {
									$note = 'Night';
								}
							}
							
							echo '<tr><td class ="check_booking_cell_left">Time in</td><td class="check_booking_cell_' . $box_status . '">'  . $_GET["hour"] . ':' . $_GET["minute"] . '</td><td class="check_booking_cell_note">' . $note . '</td></tr>';
							
							
							$box_status = 1;
							$note 		= 'OK';
								
							if ($_GET["ext"]){
								echo '<tr><td class ="check_booking_cell_left">Extension</td><td class="check_booking_cell_' . $box_status . '">'  . $_GET["ext"] . '</td><td class="check_booking_cell_note">' . $note . '</td></tr>';
							}
							
							
							$box_status = 1;
							$note 		= 'OK';
								
							if($_GET["disc"]){
								if($_GET['service'] !=60){
									$note = 'David only gets discount on 1 hour bookings';
									$box_status = 0;
									$job_errors += 1;
								}
								elseif ($_GET['service'] == 60){
									$note = 'David\'s discount';
									$box_status = 1;
								}
								
								echo '<tr><td class ="check_booking_cell_left">Discount</td><td class="check_booking_cell_' . $box_status . '">'  . $_GET["disc"] . '</td><td class="check_booking_cell_note">' . $tick . $note . '</td></tr>';
							}
							echo '</table>';
							
							if($box_status){
									$tick = '<img src="pix/nav/blue.png" width="50" height="50">&nbsp;&nbsp;';
								}
								else {
									$tick = '<img src="pix/nav/red.gif" width="30" height="30">&nbsp;&nbsp;';
								}
							
							date_default_timezone_set('Pacific/Auckland');


							
							
							if(!$job_errors){
							
								// write_log('No errors this time');
								
								echo '<p class="medtext">' . $tick . '<a href= "' . $_SERVER['PHP_SELF'] . '?id=confirm:girl_id' . $_GET["girl_id"] . '::service' . $_GET["service"] . '::cash' . $_GET["cash"] . '::eft' . $_GET["eft"] . '::credit' . $_GET["credit"] . '::hour' . $_GET["hour"] . '::minute' . $_GET["minute"] . '::ext' . $_GET["ext"] . '::disc' . $_GET["disc"] . '::girlsname' . $_GET["girlsname"] . ':">Confirm</a> this job.</p>';
							}
							else {
								if($job_errors > 1){
									$were 	= 'were';
									$s		= 's';
								}
								else {
									$were 	= 'was';
									$s 		= '';
								}
							
								echo '<p class ="medtext">There ' . $were . ' ' . $job_errors . ' error' . $s . '. <a href= "' . $_SERVER['PHP_SELF'] . '?id=delete">Start again</a>.</p>';
								echo '<hr>';
								write_log('One or more errors this time');
							}
						
						} 
						

						
						if (strstr($_GET["id"], 'confirm')){
						
							//	If the booking has been confirmed
							//	we need to enter the details into the database
							//	first extract the details
							
							$pat  = '/:girl_id[0-9]+/';
							preg_match($pat, $_GET['id'], $match);
							$girl_id = substr($match[0],8);
							
							$type = 'job';
							
							$pat  = '/:service[0-9]+/';
							preg_match($pat, $_GET['id'], $match);
							$service_desc = substr($match[0],8);
							
							$pat  = '/:cash[0-9]+/';
							preg_match($pat, $_GET['id'], $match);
							$cash = substr($match[0],5);
							
							$pat  = '/:eft[0-9]+/';
							preg_match($pat, $_GET['id'], $match);
							$eft = substr($match[0],4);
							
							$pat  = '/:credit[0-9]+/';
							preg_match($pat, $_GET['id'], $match);
							$credit = substr($match[0],7);
							
							$pat  = '/:hour[0-9]+/';
							preg_match($pat, $_GET['id'], $match);
							$hour = substr($match[0],5);
							
							$pat  = '/:minute[0-9]+/';
							preg_match($pat, $_GET['id'], $match);
							$minute = substr($match[0],7);
							
							$pat  = '/:ext[0-9]+/';
							preg_match($pat, $_GET['id'], $match);
							$ext = substr($match[0],4);
							
							$pat  = '/:disc[0-9]+/';
							preg_match($pat, $_GET['id'], $match);
							$disc = substr($match[0],5);
							
							//	Get the girl's name
							$gp = get_gp($girl_id);
							$girlsname = $gp['name'];
							
							

							//	Get the admin details
							$admin = get_admin();
							if($hour >= $admin['day_shift_start'] && $hour < $admin['night_shift_start']){
								$shift = 'day';
							}
							else {
								$shift = 'night';
							}
							
							// there is a potential problem here :-
							//	when a job is entered prior to midnight for a booking
							//	after midnight - it will be entered as belonging to the 
							//	previous date.
							
							$shift_date_code = $_SESSION['shift_date_code'];
							
							//	must update this to use $_SESSION['shift_which']
							if($shift == 'day' && $type == 'job'){	
								$pay = $admin['t_' . $service_desc . '_dy'];
							}
							else {
								$pay = $admin['t_' . $service_desc . '_nt'];	   
							}
							
							// If the girl has paid a shift fee for the current shift
							//	don't charge her a shift fee
							//	Otherwise
							//	charge her a shift fee
							
							$query = 'select * FROM jobs WHERE girl_id = "' . $girl_id . '" AND shift_date = "' . $shift_date_code . '" AND service_desc = "shift"';			
							$result = wcallq($query);
							if($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
								echo '<p>Found a shift fee</p>';
							}
							else{
								
								$query = "INSERT INTO `cgirls`.`jobs` (`job_id`, `type`, `service_desc`, `shift_date`, `adminref`, `hour`, `minute`, `girl_id`, `cash`, `eft`, `credit`, `pay_out`) VALUES (NULL, 'ite', 'shift', '$shift_date_code', '1', '$hour', '$minute', '$girl_id', 'NULL', 'NULL', 'NULL', '-10');";
							
								$result = wwopq($query);
								if (!$result) {
									die ('Could not query to the database : <br /> ' . mysql_error());
								}
								else {
									echo '<p>Shift fee entered successfully</p>';
								}
							}
		
							
							$query = "INSERT INTO `cgirls`.`jobs` (`job_id`, `type`, `service_desc`, `shift_date`, `adminref`, `hour`, `minute`, `girl_id`, `cash`, `eft`, `credit`, `pay_out`) VALUES (NULL, '$type', '$service_desc', '$shift_date_code', '1', '$hour', '$minute', '$girl_id', '$cash', '$eft', '$credit', '$pay');";
							
							$result = wwopq($query);
							if (!$result) {
								die ('Could not query to the database : <br /> ' . mysql_error());
							}
							else {
								echo '<p>Job entered successfully</p>';
								
								
								$by = $_SESSION['first_name'];
								write_log('By : ' . $by . ' |  Shift date_code : ' . $shift_date_code . ' | Type : ' . $type . ' | Service_des : ' . $service_desc . ' | Time : ' . $hour . ':' . $minute . ' | Girl_id : ' . $girl_id . ' | Girl_name : ' . $girlsname . ' | Cash : ' . $cash . ' | EFT : ' . $eft . ' | Credit : ' . $credit . ' | Pay_out : ' . $pay);

								
							}
							
							//	Initialize the girl_id to avoid re-entering the record by mistake
							$girl_id = '';

							
							echo '<hr>';

						}
						
						
						elseif (strstr($_GET["id"], 'payslip')){
						
						//	Initialize the girl_id to avoid re-entering the record by mistake
							$girl_id = '';

							
							echo '<p>Select a name to see the pay slip</p>';
							?>
							<form action="job.php" method="get">
								<select name = "girl_id">
									<option value selected>Select girl</option>
									<?php
									// Get the names of girls on shift
									//	for the drop down list
									$query = 'SELECT name, girl_id FROM girls WHERE on_off = 1';
									$result = wcallq($query);
									if ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
										$result = wcallq($query);
										while($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
											echo '<option value="' . $row['girl_id'] . '">' . $row['name'] . '</option>';
										}
									}
									?>
								</select>
								<input type = "text" name = "datecode" value = "20100615" />
								
								<input name = "payslip" type = "submit" value = "Pay slip" />
								</form> 
								<?php

						}



						elseif (strstr($_GET["payslip"], 'Pay slip')){
								//	this is a first attempt at compiling the girl's pay slip
								
								//	Must get the sort order of entries right
								
								// I think it should tally up all the jobs with a sub-total
								//	then add up the minuses - with a sub-total
								//	thwen the net amount paid out
								
								
								$shift_date_code = $_GET["datecode"];
								$girl_id = $_GET["girl_id"];
								
								// Extract the shift date from the shift_date_code

								$dt = substr($shift_date_code, 6, 2);
								$mn = substr($shift_date_code, 4, 2);
								$yr = substr($shift_date_code, 0, 4);
								$shift_date = $dt . ' - ' . $mn . ' - ' . $yr;
								
								//	Get the girl's name
								 $gp = get_gp($girl_id);
								
								
								$query = 'select * FROM jobs WHERE girl_id = "' . $girl_id . '" AND shift_date = "' . $shift_date_code . '"';			
								$result = wcallq($query);
							
								$payslip = '';
								
								//	Print girl's name
								$payslip .= '<hr><table class = "payslip_table">';

								$payslip .= '<tr><td class = "payslip_header">' . $gp['name'] . '</td><td class = "payslip_cell">Shift date :</td><td class = "payslip_header">' . $shift_date . '</td></tr>
								<tr><td class = "payslip_cell">Time</td><td class = "payslip_cell">Description</td><td class = "payslip_cell">+ / -</td></tr>';
								$i = 0;
								$girl_net_pay = 0;
								
								while($result_row = mysql_fetch_array($result, MYSQL_ASSOC)) {	
									$payslip .= ' <tr>';
									$job[$i]['job_id']	=	$result_row['job_id'];
									//	$payslip .= '<td>' . $job[$i]['job_id'] . '</td>';
									$job[$i]['girl_id']	=	$result_row['girl_id'];
									// $payslip .= '<td>' . $job[$i]['girl_id'] . '</td>';
									$job[$i]['type']		=	$result_row['type'];
									//	$payslip .= '<td>' . $job[$i]['type'] . '</td>';
									
									if(strlen($result_row['hour']) < 2){
										$job[$i]['hour']		=	'0' . $result_row['hour'];
									}
									else {
										$job[$i]['hour']		=	$result_row['hour'];
									}
									if(strlen($result_row['minute']) < 2){
										$job[$i]['minute']		=	'0' . $result_row['minute'];
									}
									else {
										$job[$i]['minute']		=	$result_row['minute'];
									}
									
									
									$payslip .= '<td class="payslip_cell">' . $job[$i]['hour'] . ':' . $job[$i]['minute'] .'</td>';
									$job[$i]['service_desc']		=	$result_row['service_desc'];
									$payslip .= '<td class="payslip_cell">' . $job[$i]['service_desc'] . '</td>';
									$job[$i]['shift_date']		=	$result_row['shift_date'];
									//	$payslip .= '<td>' . $job[$i]['shift_date'] . '</td>';
									$job[$i]['pay_out']		=	$result_row['pay_out'];
									$payslip .= '<td class="payslip_cell">' . $job[$i]['pay_out'] . '</td>';
									$payslip .= '</tr>';
									$girl_net_pay += $job[$i]['pay_out'];
									$i++;
								
								}
								
								$payslip .= '<tr><td class="payslip_cell"></td><td class="payslip_cell">Net pay = $</td></td><td class="payslip_cell">' . $girl_net_pay . '</td></tr>';
								
								
								
								$payslip .= '<tr><td class="payslip_cell">Orchestral Holdings Ltd</td><td class="payslip_cell">PO Box 29</td><td class="payslip_cell">Cambridge 3450</td></tr>';

								
								
								
								$payslip .= '</table><hr>';
								echo $payslip;
							}
						//	If the form has not been submitted
						//	Just show the form
						else {
						?>
			</td>
		</tr>
	</table>
	
	<form action="job.php" method="get">
	
		<select name = "girl_id">
			<option value selected>Select girl</option>
			<?php
				// Get the names of girls on shift
				//	for the drop down list
				$query = 'SELECT name, girl_id FROM girls WHERE on_off = 1';
				$result = wcallq($query);
				if ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
					$result = wcallq($query);
						while($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
						echo '<option value="' . $row['girl_id'] . '">' . $row['name'] . '</option>';
						
					}

				}
			?>
		</select>
		
		<br>
		<br>
		
		<select name = "service">
		<option value selected>Select service</option>
		<option value="60">60</option>
		<option value="45">45</option>
		<option value="30">30</option>
		<option value="20">20</option>
		<option value="bid">Bi double</option>
		</select>
		
		<br>
		<br>
		
		<label>Cash<input type = "text" name = "cash" value = "0" /></label>
		
		<br>
		<br>
		
		<label>EFT<input type = "text" name = "eft" value = "0" /></label>
		
		<br>
		<br>
		
		<label>Credit<input type = "text" name = "credit" value = "0" /></label>
		
		<br>
		<br>
		
		<select name = "hour">
		<option value selected>Hour</option>
		<option value="10">10</option>
		<option value="11">11</option>
		<option value="12">12</option>
		<option value="13">13</option>
		<option value="14">14</option>
		<option value="15">15</option>
		<option value="16">16</option>
		<option value="17">17</option>
		<option value="18">18</option>
		<option value="19">19</option>
		<option value="20">20</option>
		<option value="21">21</option>
		<option value="22">22</option>
		<option value="23">23</option>
		<option value="00">00</option>
		<option value="01">01</option>
		<option value="02">02</option>
		<option value="03">03</option>
		<option value="04">04</option>
		<option value="05">05</option>
		<option value="06">06</option>
		<option value="07">07</option>
		<option value="08">08</option>
		<option value="09">09</option>
		</select>
		
		<br>
		<br>
		
		<select name = "minute">
		<option value selected>Minutes</option>
		<option value="00">00</option>
		<option value="05">05</option>
		<option value="10">10</option>
		<option value="15">15</option>
		<option value="20">20</option>
		<option value="25">25</option>
		<option value="30">30</option>
		<option value="35">35</option>
		<option value="40">40</option>
		<option value="45">45</option>
		<option value="50">50</option>
		<option value="55">55</option>
		</select>
		
		<br>
		<br>
		
		<select name = "ext">
		<option value selected>Normal / extension</option>
		<option value="0">Normal</option>
		<option value="1">Extension</option>
		</select>
		
		<br>
		<br>
		
		<select name = "disc">
		<option value selected>No discount</option>
		<option value="0">None</option>
		<option value="1">David</option>
		</select>
		
		<br>
		<br>
		<?php
		echo '<input name = "girlsname" type = "hidden" value = "' . $gp['name'] . '" />';
		?>
		<input name = "submit" type = "submit" value = "Go" />
		</form> 
		
		<p>Shift date</p>
<?php
						}
					}
	//	sayHello();
		
		write_credits();
	?>
</div>
</body>
</html>