function getImgSize() { 
    var result; 
    // make sure browser supports img element objects 
    if (document.images) { 
        //	initialize return value so we can add to it 
        //	result = 0; 
        // loop through all img objects on the page 
        for (var i = 0; i < document.images.length; i++) { 
           
            document.write ("document.images[" + i + "]&nbsp;&nbsp;&nbsp;&nbsp;DOM width : " + document.images[i].width);
            document.write ("&nbsp;&nbsp;");
            document.write ("&nbsp;&nbsp;DOM height : " + document.images[i].height);
            document.write ("&nbsp;&nbsp;Name : " + document.images[i].name)
            document.write ("<br />"); 
        } 
    } 
    //	returned value is either a number or null 
    //	return result; 
}

// ****************************************************


function toggle(div_id) {
	var el = document.getElementById(div_id);
	if ( el.style.display == 'none' ) {
		el.style.display = 'block';
	}
	else {
		el.style.display = 'none';
	}
}

function blanket_size(popUpDivVar) {
	if (typeof window.innerWidth != 'undefined') {
		viewportheight = window.innerHeight;
	} else {
		viewportheight = document.documentElement.clientHeight;
	}
	if ((viewportheight > document.body.parentNode.scrollHeight) && (viewportheight > document.body.parentNode.clientHeight)) {
		blanket_height = viewportheight;
	} else {
		if (document.body.parentNode.clientHeight > document.body.parentNode.scrollHeight) {
			blanket_height = document.body.parentNode.clientHeight;
		} else {
			blanket_height = document.body.parentNode.scrollHeight;
		}
	}var blanket = document.getElementById('blanket');
	blanket.style.height = blanket_height + 'px';
	var popUpDiv = document.getElementById(popUpDivVar);
	popUpDiv_height=blanket_height/2-400;	//150 is half popup's height
	popUpDiv.style.top = popUpDiv_height + 'px';
}

function window_pos(popUpDivVar) {
	if (typeof window.innerWidth != 'undefined') {
		viewportwidth = window.innerHeight;
	} else {
		viewportwidth = document.documentElement.clientHeight;
	}
	if ((viewportwidth > document.body.parentNode.scrollWidth) && (viewportwidth > document.body.parentNode.clientWidth)) {
		window_width = viewportwidth;
	} else {
		if (document.body.parentNode.clientWidth > document.body.parentNode.scrollWidth) {
			window_width = document.body.parentNode.clientWidth;
		} else {
			window_width = document.body.parentNode.scrollWidth;
		}
	}
	var popUpDiv = document.getElementById(popUpDivVar);
	window_width=window_width/2-250;	//250 is half popup's width
	popUpDiv.style.left = window_width + 'px';
}function popup(windowname) {
	blanket_size(windowname);
	window_pos(windowname);
	toggle('blanket');
	toggle(windowname);		
}

// ****************************************************


function rollover(imgName, imgSrc) { 
    if (document.images) { 
        document.images[imgName].src = imgSrc; 
    } 
} 