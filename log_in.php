<?php
session_start();
require_once('includes/db_worx.php');
require_once('includes/cgops.php');
require_once('includes/formhelpers.php');
$logged_in = 0;
	$pix = get_pix();	
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title>Calendar Girls - Operator log in</title>
<link rel=stylesheet type="text/css" href="style/cgirlsstyle.css">

</head>
<body onload="document.login.username.focus()">

<div align="left" class="loginbox">

		<tr>
			<td>
			<?php
			if ($_GET['id'] == 'logout'){						//	User has logged out
				session_unset();
			}
				
			if($_SESSION['username']) {							//	User is already logged in
				$logged_in = 1;
			}

			elseif(empty($_SESSION['username'])) {
					
				if($_POST['username']){							//	If a username has been entered
																//	get the details for that name from the database
					$query = 'SELECT * FROM reception WHERE username = "' . $_POST['username'] . '"';
					$result = wcallq($query);
					while ($result_row = mysql_fetch_array($result, MYSQL_ASSOC)){
						$pwd 		= $result_row['password'];
						$first_name = $result_row['first_name'];
						$last_name 	= $result_row['last_name'];
					}
					
					if ($pwd == md5($_POST['password'])){	//	If the password is correct
						$logged_in = 1;
						$_SESSION['username'] = $_POST['username'];	//	Set $_SESSION['username']
						$_SESSION['first_name'] = $first_name;		//	Set $_SESSION['first_name']
						$_SESSION['shift_which'] = $_POST['shift_which']; // set $_SESSION['shift_which']
						
						//	if user is logging in to shift_which 1 or 2 shift_date_code is today
						//	otherwise it is yesterday
						
						// must get rid of $shift_date_code in favour of $_SESSION['shift_date_code']
						date_default_timezone_set('Pacific/Auckland');
							if($_SESSION['shift_which'] < 3){
								$shift_date_code = date('Ymd');
								$date_of_shift = date('d/m/Y', time());
								$_SESSION['shift_date_code'] = date('Ymd');
							}
							else {
								$shift_date_code = date('Ymd', time() - 86400);
								$date_of_shift = date('d/m/Y', time() - 86400);
								$_SESSION['shift_date_code'] = date('Ymd', time() - 86400);
							}
						
						$log = '';
						$log .= $_SESSION['first_name'] . ' successfully logged in to shift number ' . $_SESSION['shift_which'];
						write_log($log);
					}
					else {
						$try = 1;
						
					}	

				}
			}
					
			if ($logged_in){
				menubox();
			}
			
			else{
				if($try){
					echo '<br /><br /><p class = "xtx">Wrong username and / or password<br /><br />Try again</p>';
					$_POST = '';
				}
		?>
		<table border = "0" align = "center">
			<tr>
				<td>
					<div class="photobox"><?php
						write_pic($pix['$index_left']);
					?></div>
				</td>
				<td>
					<p class="venuetext"></p>
					<?php
				echo '		<table border ="0" align = "center">
								<tr>
									<td class = "logintext">';
				echo '<form method="POST" action="'. LOGIN .'" name = "login">';  
				print 'User name : 
									</td>
									<td class = "logintext">'; 
				input_text('username', $_POST); 
				echo '
									</td>
								</tr>
								<tr>
									<td class = "logintext">';
				echo 'Password : 
									</td>
									<td class = "logintext">
									';
				input_password('password', $_POST); 

				
				echo '</td>
								</tr>

								<tr>
									<td>
									</td>
									<td class = "logintext">'; 
				input_submit('submit','Log In'); 
				echo '<input type="hidden" name="_submit_check" value="1"/>
									<br /><br /><br /><br /></td>
								</tr>
								'; 
				echo '</form>
							</table>
						';


			}
	
			?>
				</td>
			</tr>
		</table>

		
			</td>
		</tr>

	

<?php
		write_credits();
	?>
</div>
</body>
</html>