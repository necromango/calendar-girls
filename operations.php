<?php

session_start();
require_once('includes/db_worx.php');
require_once('includes/cgops.php');
require_once('includes/formhelpers.php');
$on_shift_list = 0;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title>California Girls</title>
<link rel=stylesheet type="text/css" href="style/cgirlsstyle.css">

</head>
<body>

<div align="left" class="mainbox">
	<?php
		if(!$_SESSION['username']) {		//	If the user is not logged in
											//	Just give them a link to the log in page	
			echo	'<p class = "centre_cell"><a href="log_in.php">Log in</a></p>';
		}
		else{		//	Otherwise - carry on ...
			menubox();
	?>
		
		<!-- This content should probably reside somewhere else -->
		<div style ="
			margin		:	20px 20px 20px 40px;
			font-family	:	verdana;
			font-size	:	1.2em;
			background	:	#FFFCAB;
			color		:	#333;
			padding		:	40px;
			line-height	:	180%;
			box-shadow: 5px 5px 3px #AAAAAA;
			">
			
			
			
			<h1 style ="
			color		:	#333;
				">
				Notes for reception
			</h1>
			
			<span style="
				float		:	right;
				font-size	:	.8em;
				">
				Monday 6th January 2014
			</span>
			<h3 style ="
			color		:	#333;
				">
				Opening hours
			</h3>
			<p style="
				margin		:	0 0 0 40px;
				">
				
				Receptionists - <strong>start time</strong> is now 7:00PM - not 6:30PM
				<br />
				<br />
				Pay rates for shifts are as follows
				
				<table style="margin:auto;">
				<tr><th></th><th>Open</th><th>Close</th><th>Pay</th></tr>
				<tr><td style="padding : 5px 20px"><strong>Monday</strong></td><td>7:00 PM</td><td style="padding : 5px 30px">2:00 AM</td><td>120</td></tr>
				<tr><td style="padding : 5px 20px"><strong>Tuesday</strong></td><td>7:00 PM</td><td style="padding : 5px 30px">2:00 AM</td><td>120</td></tr>
				<tr><td style="padding : 5px 20px"><strong>Wednesday</strong></td><td>7:00 PM</td><td style="padding : 5px 30px">3:00 AM</td><td>130</td></tr>
				<tr><td style="padding : 5px 20px"><strong>Thursday</strong></td><td>7:00 PM</td><td style="padding : 5px 30px">4:00 AM</td><td>140</td></tr>
				<tr><td style="padding : 5px 20px"><strong>Friday</strong></td><td>7:00 PM</td><td style="padding : 5px 30px">5:00 AM</td><td>150</td></tr>
				<tr><td style="padding : 5px 20px"><strong>Saturday</strong></td><td>7:00 PM</td><td style="padding : 5px 30px">5:00 AM</td><td>150</td></tr>
				<tr><td style="padding : 5px 20px"><strong>Sunday</strong></td><td>7:00 PM</td><td style="padding : 5px 30px">2:00 AM</td><td>120</td></tr>
				</table>
				
				
			</p>
			
			<hr />
			
			
			<span style="
				float		:	right;
				font-size	:	.8em;
				">
				Friday 25th October 2013
			</span>
			<h3 style ="
			color		:	#333;
				">
				Out calls
			</h3>
			<p style="
				margin		:	0 0 0 40px;
				">
				Please describe the ladies accurately, and be honest.
				<br />
				Recently we have had clients turning down girls who arrive for an outcall because the description given by the receptionist has been inaccurate.
				<br />
				If the client <strong>does</strong> decline to proceed with the booking once he has seen the girl in person, the <strong>only</strong> money which needs to be paid by the client is the driver's fee, and the client should be made fully aware of this possibility <strong>before</strong> the booking is confirmed.
				<br />
				This is one of the reasons why it would be a good idea for <strong>all</strong> girls to have photographs on the web site.
				<br />
				Receptionists should <strong>always</strong> promote the web site; it is a fool proof way of showing the client who is on and what they look like.
			</p>
			
			<hr />
							
			<span style="
				float		:	right;
				font-size	:	.8em;
				">
				Tuesday 22nd October 2013
			</span>
			<h3 style ="
			color		:	#333;
				">
				Here is a reminder:
			</h3>
			<p style="
				margin		:	0 0 0 40px;
				">
					
					<br />
					<strong>Everybody</strong> is here to work
			</p>
			<br />
			
			<ul style="
				margin		:	0 0 0 40px;
				">
				<li>Nobody is to sleep in the lounge - If you need to sleep - go home</li>
				
			</ul>
			<br />
			
			<p style="
				margin		:	0 0 0 40px;
				">
					The focus <strong>must</strong> be on the customer - this is the only way the business can thrive
					<br />
					Customers must be made to feel welcome <strong>at all times</strong>
					<br />
					If girls are busy, sit the customer down, offer him a coffee, talk to him
					<br />
					This will make him much more likely to book in when someone becomes available.
					<br />
					- and you'll be more likely to get your bonus. 
			</p>
			
			<hr />

		
		
</div>		
		
		
		
					
	<?php
		}
	?>

	<?php
		write_credits();
	?>

</body>
</html>