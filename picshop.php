<?php

session_start();
require_once('includes/db_worx.php');

require_once('includes/cgops.php');
require_once('includes/cgpicops.php');
require_once('includes/formhelpers.php');
$on_shift_list = 0;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title>Calendar Girls</title>
<link rel=stylesheet type="text/css" href="style/cgirlsstyle.css">
<script type="text/javascript" src="js/cg.js"></script>

<?php
//	loads the tootips for the page
	//$tips = get_tips_by_name($_SERVER['PHP_SELF']);

$pstype = 'pro';
$location = 'picshop';
	if (strstr($_GET['assign_pic'], 'assign_pic')){
		$assign = true;
		$girl_id = $_GET['girl_id'];
		
		$gp = get_gp($girl_id);	
	}	
	elseif (strstr($_GET['move_pic'], 'move_pic')){
		$move = true;
		$girl_id = $_GET['girl_id'];
		$from = $_GET['pic_from'];
		$to = $_GET['move_to'];
		$gp = get_gp($girl_id);	
		
		move_pic($gp, $from, $to);
	}
	elseif (strstr($_GET['del_main_pic'], 'del_main_pic')){
		$del_main_pic = true;
		$girl_id = $_GET['girl_id'];

		//	$gp = get_gp($girl_id);	
		
		del_main_pic($girl_id);
	}
	elseif (strstr($_GET["id"], 'del_pic')) {
		
		//	We are going to delete a record
		$pat  = '/:[0-9]+/';
		preg_match($pat, $_GET['id'], $match);
		$girl_id = substr($match[0],1);
		$pat  = '/\|[0-9]+/';
		preg_match($pat, $_GET['id'], $match);
		$pic_2_del = substr($match[0],1);
		del_pic($girl_id, $pic_2_del);
		
		$gp = get_gp($girl_id);
	}
	elseif (strstr($_GET["id"], 'set_main:')) {
		
		//	We are going to set the main picture
		$pat  = '/:[0-9]+/';
		preg_match($pat, $_GET['id'], $match);
		$girl_id = substr($match[0],1);
		$pat  = '/\|[0-9]+/';
		preg_match($pat, $_GET['id'], $match);
		$pic_2_set = substr($match[0],1);
		
		$gp = get_gp($girl_id);
		
		//	echo '<p>We are going to set ' . $gp['name'] . '\'s pic number ' . $pic_2_set . ' as her main picture.</p>';
		set_main($gp, $pic_2_set);
		
		
	}
	else {	
		$girl_id = $_GET['id'];
		$gp = get_gp($girl_id);
	}
	
	
		if($assign){	
			assign_pic ($gp);		
		}
		$gp = get_gp($girl_id);
	
	
	if ($gp['has_pix']){
	$pic_num = 	writePst($pstype, $gp);
}
?>

</head>
<body>

<div align="left" class="mainbox">
	<?php
		if(!$_SESSION['username']) {		//	If the user is not logged in
											//	Just give them a link to the log in page	
			echo	'<p class = "centre_cell"><a href="log_in.php">Log in</a></p>';
		}
		else{								//	Otherwise - carry on ...
			menubox();
		}
		

	

		


	?>
	
	
			
<!-- ****************************************************** -->
	<?php
	
		echo '<h1>' . $gp['name'] . '\'s pic shop page</h1>';	
		
		echo '<p class = "psht">This is where you can upload your pictures for the website.<br />
		Start by clicking on \'Choose file\', then, when the little file icon appears next to the \'Choose file\' button, click on \'Submit\'.<br />
		(You may need to click on this button twice).<br />
		A small version of the picture will appear on the web page.<br />
		Go to the drop-down menu and choose a number from 1 to 10 where you want the picture to go in your photostrip (you can change this later).<br />
		Click on \'Go\'<br />
		Continue until you have loaded all your pictures - up to a maximum of 10.<br />
		You will now see that you can \'delete\', \'set as main\', or \'move\' each of your pictures.<br />
		At the bottom of the page you will se a preview of what your photostrip will look like.<br /> 
		This behaves just as it will on your page - try it.<br /><br />
		Sometimes the picture won\'t update when you change something - or you seem to get the wrong picture in the wrong place - When this happens Reload the page in your browser.</p>';
		
		
				echo '<hr class = "picshop_hr"><span class = "psht"><a href="edit_who.php?id=ed_g' . $gp['girl_id'] . '">Back</a> to ' . $gp['name'] . '\'s details page</span>';



		//	draws the box that does the business
		picshop_index($gp);
		
		// tests for the existence of $gp['girl_id']'s image directories 
		//	tells you if they are there
		//	makes them and then tells you what it has done if they are not
		
		est_img_dir($gp);
		
		
		
		photoStrip($location, $pstype, $girl_id, $gp, $pic_num, $preview);
		
		
		
		
		
		
		
		

		
		

	?>
	
	



<!-- ****************************************************** -->	
				
				
	<?php
		write_credits();
	?>

</body>
</html>